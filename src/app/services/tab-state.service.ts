import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TabStateService {

  public display: boolean = true;

  constructor() { }

  setDisplay(display: boolean){
    this.display = display;
  }
}
