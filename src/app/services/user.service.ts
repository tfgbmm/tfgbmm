import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { UtilsService } from './utils.service';
import { ClientModel } from '../schemes/models/client.model';
import { Observable} from 'rxjs';
import { take } from 'rxjs/operators';
import { MessageModel } from '../schemes/models/message.model';
import { stateMessageModel } from '../schemes/enums/stateMessage.enum';
import { BusinessModel } from '../schemes/models/business.model';
import { EmployeeModel } from '../schemes/models/employee.model';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  registerCompleted: boolean;
  tokenFacebookGoogle: string;
  
  constructor(
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private utilsService: UtilsService
    ) { }
  
  createUser(user: ClientModel | BusinessModel, employee?: EmployeeModel): Promise<void[] | void>{
    if(user.details.rol == "C") {
      return this.db.object(`users/${user.id}`).update(user);
    }else{
      return Promise.all([
        this.db.object(`users/${user.id}`).update(user), 
        this.db.object(`users/${user.id}/idEmployees/${employee.id}`).set(employee.id),
        this.db.object(`business/${user.id}`).set(user.id),
        this.db.object(`employees/${employee.id}`).set(employee)]
        );
    }  
  }

  createUserRRSS(id: string, user, employee?: EmployeeModel): Promise<void[] | void>{
    if(user.rol == "C") {
      return this.db.object(`users/${id}/details`).update(user);
    }else{
      return Promise.all([
        this.db.object(`users/${id}/details`).set(user),
        this.db.object(`users/${id}/idEmployees/${employee.id}`).set(employee.id),
        this.db.object(`business/${id}`).set(id),
        this.db.object(`employees/${employee.id}`).set(employee)
      ]);
    }
  }

  getUserById(id: string): Promise<any>{
    return this.db.object<ClientModel | BusinessModel>(`users/${id}`).valueChanges().pipe(take(1)).toPromise();
  }

  getBusinessById(id: string): Promise<BusinessModel> {
    return this.db.object<BusinessModel>(`users/${id}`).valueChanges().pipe(take(1)).toPromise();
  }

  deleteBlockedUser(idUser: string, idBlockedBusiness: string){
    return this.db.object(`users/${idUser}/idBlockedBusiness/${idBlockedBusiness}`).remove();
  }

  getRolUser(id: string): Promise<string>{
    return this.db.object<string>(`users/${id}/details/rol`).valueChanges().pipe(take(1)).toPromise();
  }

  setMessageFromUser(idUser: string, idMsg: string, state: stateMessageModel): Promise<void>{
    return this.db.object<stateMessageModel>(`users/${idUser}/messages/${idMsg}/state`).set(state);
  } 

  getMessagesFromUserCount(idUser: string): Observable<MessageModel[]>{
    return this.db.list<MessageModel>(`users/${idUser}/messages`).valueChanges();
  }

  getMessagesFromUser(idUser: string): Promise<MessageModel[]>{
    return this.db.list<MessageModel>(`users/${idUser}/messages`).valueChanges().pipe(take(1)).toPromise();
  }

  updateMessagesFromUser(idUser: string, message: MessageModel) : Promise<void>{
    return this.db.object<MessageModel>(`users/${idUser}/messages/${message.id}`).update(message);
  }

  updateEditProfile(user, email: string): Promise<void>{
    if(email == user.email) { 
      return this.db.object(`users/${user.id}`).update(user);
    }else{
      return this.updateEmail(user.email);
    }
  }

  async updateEmail(newEmail): Promise<void>{
    try {
      return this.afAuth.auth.currentUser.updateEmail(`${newEmail}`);
    }
    catch (e) {
      return await this.utilsService.errorHandling(e);
    }
  }

  updateEditProfileBD(user, employeesUpdate): Promise <[void, void] | void>{
    return (employeesUpdate != '1') ?
      Promise.all([this.db.object(`users/${user.id}`).update(user), employeesUpdate]) : 
      this.db.object(`users/${user.id}`).update(user);
  }

  deleteMsg(id: string, idMessage: string) {
    return this.db.object(`users/${id}/messages/${idMessage}`).remove();
  }

  createUserFacebookOrGoogle(userCredential: firebase.auth.UserCredential){
    return this.db.object(`users/${userCredential.user.uid}`).update({
      id: userCredential.user.uid, 
      email: userCredential.user.email, 
      details: {
        fullname: userCredential.user.displayName, 
        imgProfile: "https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Fuser.jpg?alt=media&token=5b9d4e01-8974-411b-95f9-bd2d88c41c73"
      }
    });
  }

  updateUsersFavourites(idUser: string, listFavourites: string[]): Promise<void>{
    return this.db.object<any>(`users/${idUser}/idFavourites`).set(listFavourites);
  }

  getIdsBusiness(){
    return this.db.list<string>(`business`).valueChanges().pipe(take(1)).toPromise();
  }

  getEmployeeById(id: string){
    return this.db.object<any>(`employees/${id}`).valueChanges().pipe(take(1)).toPromise();
  }

  deleteEmployee(id: string, idBusiness: string, services) : Promise<[void, void, void]>{
    return Promise.all([
      this.db.object(`employees/${id}`).remove(), 
      this.db.object(`users/${idBusiness}/idEmployees/${id}`).remove(),
      this.db.object(`users/${idBusiness}/details/services`).set(services)
    ]);
  }

  addOrEditEmployee(employee, business, tag: boolean): Promise<[void, void, void] | void> {
    return (tag) ? 
      Promise.all([
        this.db.object(`employees/${employee.id}`).update(employee), 
        this.db.object(`users/${business.id}/idEmployees/${employee.id}`).set(employee.id),
        this.db.object(`users/${business.id}/details/services`).update(business.details.services)]) :
      this.db.object(`employees/${employee.id}`).update(employee);
  }

  updateEmployee(employee: EmployeeModel){
    return this.db.object(`employees/${employee.id}`).update(employee);
  }

}


