import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AppointmentModel } from '../schemes/models/appointment.model';
import { take } from 'rxjs/internal/operators/take';
import { UtilsService } from './utils.service';
import { HttpClient } from '@angular/common/http';
import { BusinessModel } from '../schemes/models/business.model';
declare var require: any

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  

  constructor(
    private db:  AngularFireDatabase,
    private utilsService: UtilsService, 
    private http: HttpClient
    ) { }
  
  getAppointmentById(id: string): Promise<AppointmentModel>{
    return this.db.object<AppointmentModel>(`appointments/${id}`).valueChanges().pipe(take(1)).toPromise();
  }

  getBusinessAllAppointments(id: string): Promise<AppointmentModel[]>{
    return this.db.list<AppointmentModel>(`users/${id}/idAppointments`).valueChanges().pipe(take(1)).toPromise();
  }

  updateNotificationsAppointments(userID: string) {
    return this.db.list(`users/${userID}/messages`).valueChanges();
  }

  changesProfiles(idAppointment: string, idBusiness: string): Promise<[void, void]>{
      return Promise.all([
        this.db.object(`users/${idBusiness}/idAppointments/${idAppointment}`).remove(),
        this.db.object(`appointments/${idAppointment}`).remove()
      ]);
  }

  changeStateAppointments(id: string, appointmentID: string, type: string): Promise <void> | Promise<[void, void]>{
    if(type != 'A')
      return this.db.object(`users/${id}/idAppointments/${appointmentID}/state`).set('completed');
    else 
      return Promise.all([
        this.db.object(`users/${id}/idAppointments/${appointmentID}`).remove(),
        this.db.object(`appointments/${appointmentID}`).remove()
      ]);
  }

  async confirmAppointment(appointment, date: string): Promise<[void, void, void]>{
    const timestampServer = await this.utilsService.getServerTimeStamp();
    const now : number = new Date(timestampServer['timestamp']).getTime();
    const id: string = this.utilsService.generateID();

    const moment = require('moment');
    const momentDate: string = `${moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('dddd')} ${moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LL')} a las ${moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LT')}`;
    const titleMessage: string = "Pasada lista de asistencia";
    const contentMessage: string = `Se ha pasado la lista de asistencia correctamente de la cita del "${momentDate}" en la empresa "${appointment.nameBusiness}". `;
    
    const message = { 
      id: id, 
      title: titleMessage,
      date: now, 
      dateAppointment: appointment.date,
      state: 'unread',
      content: contentMessage,
      type: 'off'
    }

    const confirmAppointmentUserPromises = appointment.members.map((member) => {
      if(member.status == "TRUE-CLOSE"){
        if(date == "") { // La empresa no tiene activada el sistema de penalizaciones
          return Promise.all([
            this.db.object(`users/${member.id}/idAppointments/${appointment.id}/state`).set('not_confirmed'),
            this.db.object(`users/${member.id}/messages/${id}`).set(message)]);
        }else{
          return Promise.all([
            this.db.object(`users/${member.id}/idAppointments/${appointment.id}/state`).set('not_confirmed'),
            this.db.object(`users/${member.id}/messages/${id}`).set(message),
            this.db.object(`users/${member.id}/idBlockedBusiness/${appointment.idBusiness}`).set(appointment.idBusiness+"+"+date)
          ]);
        }
      }else{
        return Promise.all([
          this.db.object(`users/${member.id}/idAppointments/${appointment.id}/state`).set('confirmed'),
          this.db.object(`users/${member.id}/messages/${id}`).set(message)]);
      }
    });

    return Promise.all([confirmAppointmentUserPromises, 
      this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}/state`).set('confirmed'),
      this.db.object(`users/${appointment.idBusiness}/messages/${id}`).set(message)
    ]);
  }

  async createAppointment(appointment: AppointmentModel, maxVancancies: number, user, business: BusinessModel, 
    datetimeCompleted): Promise<void>{
    
    var upvotesRef = this.db.object(`appointments/${appointment.id}`);
    var lock: {exist: boolean, mutex: boolean};

    return upvotesRef.query.ref.transaction(function (current_value) {
      if(current_value != null) {
        const members = (current_value.members) ? Object.keys(current_value.members).map((key: string) => current_value.members[key]) : [];
        if((members.length == 0) || (
            members.length < maxVancancies && 
            current_value.type != 'I' && 
            current_value.type == appointment.type && 
            current_value.service.id == appointment.service.id
          )){
          current_value.members[user.id] = {id: user.id, name: user.details.fullname ? user.details.fullname : user.details.nameBusiness};
          lock = {exist: true, mutex: true};
          return current_value;
        }else{  
          lock = {exist: true, mutex: false};
          return current_value;
        }
      }else{
        lock = {exist: false, mutex: true};
        const checkMembers = (appointment.members) ? Object.keys(appointment.members).map((key: string) => appointment.members[key]) : [];
        if (checkMembers.length != 1 ) {
          appointment.members = {}; 
          appointment.members[user.id] = {id: user.id, name: user.details.fullname ? user.details.fullname : user.details.nameBusiness};
        }
        return appointment;
      }

    }).then(async () => {

      const moment = require('moment');
      const momentDate = `${moment(datetimeCompleted, 'YYYY-MM-DDThh:mm').locale('es').format('dddd')} ${moment(datetimeCompleted, 'YYYY-MM-DDThh:mm').locale('es').format('LL')} a las ${moment(datetimeCompleted, 'YYYY-MM-DDThh:mm').locale('es').format('LT')}`;
      const timestampServer = await this.utilsService.getServerTimeStamp();
      const now = new Date(timestampServer['timestamp']).getTime(); 

      const idClients = business.idClients ? this.utilsService.getArrayFromObject(business.idClients) : [];
      var whoami = idClients.some((id) => user.id == id);
      if(!whoami) {
        whoami = user.id != business.id ? true : false;
      }

      var messageClientContent: string = `Tienes una nueva reserva para el ${momentDate} en la empresa "${business.details.nameBusiness}".`;
      var messageEmployeeContent: string = `El cliente "${user.details.fullname ? user.details.fullname : user.details.nameBusiness}" te ha solicitado una nueva cita ${(appointment.type == 'G') ? 'grupal' : 'individual'} para el ${momentDate}. `;
      var messageInhabilite: string = `Has inhabilitado una reserva para el ${momentDate}.`;
      var titleInhabilite: string = `Hora inhabilitada`;

      const url = this.utilsService.getServerSendPushUrl();
      const id = this.utilsService.generateID();

      const messageClient = {
        id: id, 
        title: `Cita en ${business.details.nameBusiness}`,
        date: now, 
        dateAppointment: datetimeCompleted,
        state: 'unread',
        content: messageClientContent,
        type: 'on'
      }

      const messageEmployee = {
        id: id, 
        title: (user.id != business.id) ? `Cita de ${user.details.fullname ? user.details.fullname : user.details.nameBusiness}` : titleInhabilite, 
        date: now,
        dateAppointment: datetimeCompleted,
        state: 'unread',
        content: (user.id != business.id) ? messageEmployeeContent : messageInhabilite,
        type: (user.id != business.id) ? 'on' : 'off'
      }

      const payload = [{
          notification: {
            title: business.details.nameBusiness,
            body: messageClientContent,
            sound: "default",
            icon: "notification_icon"
          },
          data: {
            body: messageClientContent,
            type: 'notification',
            notification_foreground: 'true',
          }
        }, {
          notification: {
            title: user.details.fullname ? user.details.fullname : user.details.nameBusiness,
            body: (user.id != business.id) ? messageEmployeeContent : messageInhabilite,
            sound: "default",
            icon: "notification_icon"
          },
          data: {
            body: (user.id != business.id) ? messageEmployeeContent : messageInhabilite,
            type: 'notification',
            notification_foreground: 'true'
          }
      }];

      const body = [{
        token: user.tokenDevice,
        payload: payload[0]
      }, {
        token: business.tokenDevice,
        payload: payload[1]
      }]; 
      

      if(lock.exist && lock.mutex){
        
        if(whoami) this.db.object(`users/${appointment.idBusiness}/idClients/${user.id}`).set(user.id).catch((e) => this.utilsService.errorHandling(e));
        if(user.id != business.id) {
          return Promise.all([
            this.db.object(`users/${user.id}/messages/${id}`).set(messageClient), 
            this.db.object(`users/${user.id}/idAppointments/${appointment.id}`).set({id: appointment.id, state: 'pending', idService: appointment.service.id, type: appointment.type}),
            this.db.object(`users/${appointment.idBusiness}/messages/${id}`).set(messageEmployee),
            this.http.post(url, body[0]).toPromise(), 
            this.http.post(url, body[1]).toPromise()
            ]);
        }else{
          return Promise.all([
            this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}`).set({id: appointment.id, state: 'pending', idService: appointment.service.id, type: appointment.type}),
            this.db.object(`users/${appointment.idBusiness}/messages/${id}`).set(messageEmployee),
            this.http.post(url, body[1]).toPromise()
          ]);
        }
        
      }else if(!lock.exist && lock.mutex){

        if(whoami) this.db.object(`users/${appointment.idBusiness}/idClients/${user.id}`).set(user.id).catch((e) => this.utilsService.errorHandling(e));
        if(user.id != business.id) {
          return Promise.all([
            this.db.object(`users/${user.id}/idAppointments/${appointment.id}`).set({id: appointment.id, state: 'pending', idService: appointment.service.id, type: appointment.type}),
            this.db.object(`users/${user.id}/messages/${id}`).set(messageClient),
            this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}`).set({id: appointment.id, state: 'pending', idService: appointment.service.id, type: appointment.type}),
            this.db.object(`users/${appointment.idBusiness}/messages/${id}`).set(messageEmployee),
            this.http.post(url, body[0]).toPromise(), 
            this.http.post(url, body[1]).toPromise()
          ]); 
        }else{
          return Promise.all([
            this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}`).set({id: appointment.id, state: 'pending', idService: appointment.service.id, type: appointment.type}),
            this.db.object(`users/${appointment.idBusiness}/messages/${id}`).set(messageEmployee),
            this.http.post(url, body[1]).toPromise()
          ]);
        }
      }else{
        this.utilsService.showToast("Lo sentimos, parece que se han adelantado y ya dicha hora se encuentra ocupada. Elija otra hora y no se quede sin su cita", "bottom", "tertiary");
      }
      
    }).catch((err) => this.utilsService.errorHandling(err));
    
  }


  async cancelledAppointments(user, appointment: AppointmentModel, business: BusinessModel, now: number, state?: string): Promise<any>{

    var upvotesRef = this.db.object(`appointments/${appointment.id}`);
    var lock: {exist?: boolean, mutex?: boolean, singleMember?: boolean} = {mutex: false, singleMember: false};

    return upvotesRef.query.ref.transaction(function (current_value) {

      if(current_value != null) {
        var members = (current_value.members) ? Object.keys(current_value.members).map((key: string) => current_value.members[key]) : [];
        lock = {mutex: false, exist: true};
        if(members.length == 1){
          lock.singleMember = true;
          return null;
        }else{  
          if(current_value.idBusiness != user.id){
            members = members.filter((member) => member.id !== user.id);
            let object = {};
            current_value.members = members.map((user) => {
              return object[user.id] = user;
            });
            return current_value;
          }else{
            appointment.members = members;
            return null;
          }
        }
      }else{
        lock = {mutex: true, exist: false};
        return current_value;
      }

    }).then(async () => {
      // Preparamos las notificaciones a enviar a los usuarios
      const moment = require('moment');
      const momentDate = `${moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('dddd')} ${moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LL')} a las ${moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LT')}`;
      
      const url = this.utilsService.getServerSendPushUrl();
      const id = this.utilsService.generateID();

      const titleClient = `Cita cancelada en ${business.details.nameBusiness.split(' ')[0]}`;
      const msgClient = (appointment.idBusiness == user.id) ? `Se ha cancelado la cita prevista en ${business.details.nameBusiness} para el ${momentDate}.`: (state == "daysBefore-daysPenalitation" || state == "daysPenalitation") ?
        `Se ha cancelado la cita prevista en ${business.details.nameBusiness} para el ${momentDate}. Recuerda que ha sido penalizado por ${appointment.daysPenalitation} día/s sin poder reservar de nuevo en esta empresa y no se le devolverá el pago realizado.` :
        (state == "daysBefore") ? `Se ha cancelado la cita prevista en ${business.details.nameBusiness} para el ${momentDate}. Recuerda que no se le devolverá el pago realizado.`
        : `Se ha cancelado la cita prevista en ${business.details.nameBusiness} para el ${momentDate}.`;
      
      
      const titleBusiness = `Cita cancelada por ${(user.details.rol == 'C') ? user.details.fullname.split(' ')[0] : user.details.nameBusiness.split(' ')[0]}`;
      const msgBusiness = `El cliente llamado "${(user.details.rol == 'C') ? user.details.fullname : user.details.nameBusiness}" ha cancelado la cita prevista para el ${momentDate}.`;
      
      const titleBusiness1 = `Cita cancelada correctamente`;
      const msgBusiness1 = `Has cancelado la cita correctamente para el ${momentDate}. Ya han sido avisados todos los integrantes de la cita.`;
      
      const titleHabilitate = "Hora habilitada de nuevo";
      const msgHabilitate =  `Has habilitado de nuevo la hora para el ${momentDate}.`

      const payload = [{
        notification: {
          title: titleClient,
          body: msgClient,
          sound: "default",
          icon: "notification_icon"
        },
        data: {
          body: msgClient,
          type: 'notification',
          notification_foreground: 'true',
        }
      }, {
        notification: {
          title: titleBusiness,
          body: msgBusiness,
          sound: "default",
          icon: "notification_icon"
        },
        data: {
          body: msgBusiness,
          type: 'notification',
          notification_foreground: 'true'
        }
    }, {
      notification: {
        title: !(appointment.type == 'A') ? titleBusiness1 : titleHabilitate,
        body: !(appointment.type == 'A') ? msgBusiness1 : msgHabilitate,
        sound: "default",
        icon: "notification_icon"
      },
      data: {
        body: !(appointment.type == 'A') ? msgBusiness1 : msgHabilitate,
        type: 'notification',
        notification_foreground: 'true'
      }
    }];

    
    const messageClient = { 
      id: id, 
      title: titleClient,
      date: now, 
      dateAppointment: appointment.date,
      state: 'unread',
      content: msgClient,
      type: 'off'
    }

    const messageEmployee = { 
      id: id, 
      title: titleBusiness, 
      date: now,
      dateAppointment: appointment.date,
      state: 'unread',
      content: msgBusiness,
      type: 'off'
    }

    const messageEmployee1 = {
      id: id, 
      title: !(appointment.type == 'A') ? titleBusiness1 : titleHabilitate, 
      date: now,
      dateAppointment: appointment.date,
      state: 'unread',
      type: 'off',
      content: !(appointment.type == 'A') ? msgBusiness1 : msgHabilitate
    }

    const body = [{
      token: user.tokenDevice,
      payload: payload[0]
    }, {
      token: business.tokenDevice,
      payload: payload[1]
    }, 
    {
      token: business.tokenDevice,
      payload: payload[2]
    }]; 

    if(user.id != appointment.idBusiness) {
      if(lock.exist) {
        if(lock.singleMember) {
          if(state == "daysBefore-daysPenalitation" || state == "daysPenalitation") {
            const days = parseInt(appointment.daysPenalitation) * 86400000; 
            var date = new Date(now+days).getTime().toString();
            this.db.object(`users/${user.id}/idBlockedBusiness/${appointment.idBusiness}`)
              .set(appointment.idBusiness+"+"+date)
              .catch((e) => this.utilsService.errorHandling(e));
          }
          return Promise.all([
            this.db.object(`users/${user.id}/idAppointments/${appointment.id}`).remove(),
            this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}`).remove(),
            this.db.object(`users/${user.id}/messages/${id}`).set(messageClient),
            this.db.object(`users/${business.id}/messages/${id}`).set(messageEmployee),
            this.http.post(url, body[0]).toPromise(),
            this.http.post(url, body[1]).toPromise()
          ]);
        }else{
          if(state == "daysBefore-daysPenalitation" || state == "daysPenalitation") {
            const days = parseInt(appointment.daysPenalitation) * 86400000;
            var date = new Date(now+days).getTime().toString();
            this.db.object(`users/${user.id}/idBlockedBusiness/${appointment.idBusiness}`)
              .set(appointment.idBusiness+"+"+date)
              .catch((e) => this.utilsService.errorHandling(e));
          }

          return Promise.all([
          this.db.object(`users/${user.id}/idAppointments/${appointment.id}`).remove(),
          this.db.object(`users/${user.id}/messages/${id}`).set(messageClient),
          this.db.object(`users/${business.id}/messages/${id}`).set(messageEmployee),
          this.http.post(url, body[0]).toPromise(),
          this.http.post(url, body[1]).toPromise()]);
        }
      
      }else{
        this.utilsService.showToast("Parece que el administrador de la empresa se adelantó y ha cancelado la cita primero", "bottom", "tertiary");
      }
      

    }else{
      if(appointment.type == 'A'){
        return Promise.all([
          this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}`).remove(),
          this.db.object(`appointments/${appointment.id}`).remove(),
          this.db.object(`users/${business.id}/messages/${id}`).set(messageEmployee1),
          this.http.post(url, body[2]).toPromise()
        ]);
      }else{
        if(!lock.mutex){
          const promiseMembers = appointment.members.map(user => {
            return this.db.object(`users/${user.id}/tokenDevice`).valueChanges().pipe(take(1)).toPromise();
          });
          
          const tokenDevicesUsers : string[] = await Promise.all(promiseMembers).catch(e => this.utilsService.errorHandling(e));
  
          const notificationsPushPromises = tokenDevicesUsers.map((token: string) => {
            return this.http.post(url, {
              token: token,
              payload: payload[0]
            }).toPromise(); 
          });
  
          const messagesUserIds = appointment.members.map(user => {
            return this.db.object(`users/${user.id}/messages/${id}`).set(messageClient);
          }); 
  
          const promisesDeleteUsers = appointment.members.map((user) => {
            return this.db.object(`users/${user.id}/idAppointments/${appointment.id}`).remove();
          });
  
          return Promise.all([
            Promise.all(promisesDeleteUsers),
            this.db.object(`users/${appointment.idBusiness}/idAppointments/${appointment.id}`).remove(),
            this.db.object(`users/${business.id}/messages/${id}`).set(messageEmployee1),
            Promise.all(messagesUserIds),
            Promise.all(notificationsPushPromises),
            this.http.post(url, body[2]).toPromise()
          ]);
        }else{
          this.utilsService.showToast("El único miembro que quedaba ha cancelado la cita primero", "bottom", "tertiary");
        }
      }
    }

  }).catch((e) => this.utilsService.errorHandling(e));

  }
}