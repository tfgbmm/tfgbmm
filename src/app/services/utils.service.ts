import { Injectable } from '@angular/core';
import { LoadingController, ToastController, AlertController, ModalController } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {

    toast: HTMLIonToastElement;
    loading: HTMLIonLoadingElement;
    constructor(
        private loadingController: LoadingController,
        private toastCtrl: ToastController,
        private db: AngularFireDatabase,
        private translate: TranslateService,
        private http: HttpClient,
        private alertController: AlertController
        ){}

    generateID(): string {
        return this.db.createPushId();
    }

    getArrayFromObject(object: any): any[] {
        if (object)
            return Object.keys(object).map((key: string) => object[key]);
        return [];
    }

    getServerTimeStamp() {
        const url = 'https://us-central1-tfg-bmm.cloudfunctions.net/currentTime';
        return this.http.get(url).toPromise();
    }

    getServerSendPushUrl(){
        return 'https://us-central1-tfg-bmm.cloudfunctions.net/sendPush';
    }

    async dissmissModal(modal: ModalController){
        modal.getTop().then(v => v ? modal.dismiss({}): null);
    }

    async presentLoading() {
        const loading = await this.loadingController.create({
        message: 'Cargando...',
        spinner: 'lines',
        cssClass: 'custom-loader'
        });
        await loading.present();
        return loading;
    }
   
    async dissmissLoading(idObj?: {data?: any, id?: string, role?:string}) {
        if(!idObj) this.loadingController.getTop().then(async v => v ? await this.loadingController.dismiss() : null);
        else if(idObj.id) await this.loadingController.dismiss(undefined, undefined, idObj.id).catch(err => console.error(err));
        else this.loadingController.getTop().then(async v => v ? await this.loadingController.dismiss() : null);
    }

    async showToast(message: string, position: "top" | "bottom" | "middle" = "bottom", color: string = "dark"): Promise<void> {
        let toast = await this.toastCtrl.create({
            message: message,
            position: position,
            closeButtonText: 'OK',
            showCloseButton: true,
            color: color
        });
        await toast.present();
    }

    async errorToast(message: string, position: "top" | "bottom" | "middle" = "bottom", 
        ): Promise<void> {

        if(this.toast) await this.toast.dismiss();
        this.toast = await this.toastCtrl.create({
            message: message,
            position: position,
            closeButtonText: 'OK',
            showCloseButton: true,
            color: "danger"
        });

        await this.toast.present();
    }

    async dissmissToast(){
        if(this.toast) await this.toast.dismiss();
    }

    async showInformationAlert(header: string, message: string, button: string){
        const alert = await this.alertController.create({
            header: header,
            message: message,
            buttons: [button]
        });
        await alert.present();
    }

    async errorHandling(e) {
        let error = e;
        if (!e.code && e.error) error = e.error;
        if (error.code) {
            switch (error.code) {
                case 'auth/user-not-found':
                    this.translate.get('email_not_exists').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break;
                case 'auth/wrong-password':
                    this.translate.get('error_email_or_password').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break;
                case 'auth/network-request-failed':
                    this.translate.get('error_network').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break;
                case 'auth/email-already-in-use':
                    this.translate.get('email_exists').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break; 
                case 'auth/too-many-requests':
                    this.translate.get('too_many_requests').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break; 
                case 'auth/account-exists-with-different-credential':
                    this.translate.get('account_exists_with_different_credential').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break; 
                case 'PERMISSION_DENIED':
                    this.translate.get('permission_denied').pipe(take(1)).toPromise().then(async (data) => {
                        await this.errorToast(data);
                    });
                    break; 
                default:
                    await this.errorToast(error.message, "bottom");
                    break;

            }
        } else if(e == 7){
            await this.errorToast("Ha ocurrido un problema al iniciar sesión con la cuenta de google. Comprueba que tenga conexión a internet e inténtelo de nuevo", "bottom");
        } else {
            await this.errorToast("Ha ocurrido un problema. Por favor, reinicie la aplicación e inténtelo de nuevo", "bottom");
        }
        return null;
    }
    
}
