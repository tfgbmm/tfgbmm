import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FacebookLoginResponse, Facebook } from '@ionic-native/facebook/ngx';
import * as firebase from 'firebase/app';
import { UtilsService } from './utils.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { UserService } from './user.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private afAuth: AngularFireAuth,
    private utils: UtilsService,
    private facebook: Facebook,
    private google: GooglePlus,
    private userServ: UserService,
    private db: AngularFireDatabase
  ) { }
  
  getCurrentUserUID() {
    return this.afAuth.auth.currentUser.uid;
  }

  login(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  logout(): void {
    this.afAuth.auth.signOut();
  }

  createUser(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  resetPassword(email: string): Promise<void>{
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  async loginWithFacebook(): Promise<firebase.auth.UserCredential> {
    const facebookLoginResponse: FacebookLoginResponse = await this.facebook.login(['public_profile', 'email']).catch((e) => this.utils.errorHandling(e));
    if (!facebookLoginResponse) return null;
    const facebookCredential = firebase.auth.FacebookAuthProvider.credential(facebookLoginResponse.authResponse.accessToken);
    const userCredential: firebase.auth.UserCredential = await firebase.auth().signInWithCredential(facebookCredential).catch((e) => this.utils.errorHandling(e));
    const doc = await this.userServ.getRolUser(userCredential.user.uid);
    if (!doc) await this.userServ.createUserFacebookOrGoogle(userCredential);
    return userCredential;
  }

  async loginWithGoogle(): Promise<firebase.auth.UserCredential> {
    const googleUser: any = await this.google.login({
      'webClientId': '626168960359-c08k8o74f2svegj9ej6ospcgignu36b8.apps.googleusercontent.com',
      'scopes': 'profile email'
    }).catch((e) => this.utils.errorHandling(e));
    if (!googleUser) return null;
    const googleCredential = firebase.auth.GoogleAuthProvider.credential(googleUser.idToken);
    const userCredential: firebase.auth.UserCredential = await firebase.auth().signInWithCredential(googleCredential).catch(e => { this.utils.errorHandling(e); return null });
    const doc = await this.userServ.getRolUser(userCredential.user.uid);
    if (!doc) await this.userServ.createUserFacebookOrGoogle(userCredential);
    return userCredential;
  }

  setToken(token: string) {
    return this.db.object(`users/${this.getCurrentUserUID()}/tokenDevice`).set(token);
  }
  
}
