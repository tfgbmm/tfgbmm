import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { TranslateService } from '@ngx-translate/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private navController: NavController,
    private translate: TranslateService,
    private firebaseX: FirebaseX,
    private authService: AuthService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.translate.setDefaultLang(this.translate.getBrowserLang());
      this.statusBar.styleLightContent();
      this.statusBar.backgroundColorByHexString("#232428");
      this.afAuth.authState.subscribe(async (user: firebase.User) => {  
        
        this.firebaseX.grantPermission(); /*Plataforma iOS*/ 
        this.firebaseX.clearAllNotifications();

        if (user) {
          this.setNotifications();
          this.setClearAllNotifications();
          this.firebaseX.setCrashlyticsUserId(user.uid);
        } else {
          this.navController.navigateBack(['/login'], { replaceUrl: true });
        }
        
      });
    });
  }
  
  async setNotifications() {
    const token = await this.firebaseX.getToken();
    this.authService.setToken(token);

    this.firebaseX.onMessageReceived()
    .subscribe(data => {
      if(data.tap) return this.navController.navigateForward("/tabs/list_messages");
    });

    this.firebaseX.onTokenRefresh()
      .subscribe((token: string) => console.log(`Got a new token ${token}`));
  }

  setClearAllNotifications(){
    this.platform.resume.subscribe(async () => {
        this.firebaseX.clearAllNotifications();
    });
  }
}



