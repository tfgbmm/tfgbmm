import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage/storage';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/dev/environment.dev';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook/ngx';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { IonicSelectableModule } from 'ionic-selectable';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { MessagesPage } from './pages/messages/messages.page';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { TermsConditionsComponent } from './components/terms-conditions/terms-conditions.component';
import { ServiceComponent } from './components/service/service.component';
import { SharedModule } from './shared/shared.module';
import { MembersComponent } from './components/members/members.component';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, MessagesPage, ScheduleComponent, TermsConditionsComponent, ServiceComponent, MembersComponent],
  entryComponents: [MessagesPage, ScheduleComponent, TermsConditionsComponent, ServiceComponent, MembersComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    SharedModule,
    IonicSelectableModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireStorage,
    AngularFireDatabase,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Facebook,
    FirebaseX,
    GooglePlus,
    PhotoViewer,
    AndroidPermissions,
    Calendar,
    CallNumber,
    SMS 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
