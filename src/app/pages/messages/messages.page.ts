import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { stateMessageModel } from 'src/app/schemes/enums/stateMessage.enum';
import { AuthService } from 'src/app/services/auth.service';
import { MessageModel } from 'src/app/schemes/models/message.model';
import { Calendar } from '@ionic-native/calendar/ngx';
import { UtilsService } from 'src/app/services/utils.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.page.html',
  styleUrls: ['./messages.page.scss'],
})
export class MessagesPage implements OnInit {
  
  private message : MessageModel;
  
  constructor(
    private modalCtrl: ModalController,
    private userService: UserService,
    private authService: AuthService,
    private calendar: Calendar,
    private alertController: AlertController,
    private utilsService: UtilsService,
    private androidPermissions: AndroidPermissions
    ){}

  ngOnInit() {
    if(this.message.state == "unread") this.userService.setMessageFromUser(this.authService.getCurrentUserUID(), this.message.id, stateMessageModel.read);
  }

  async addToCalendar(){
    const permision = await this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.WRITE_CALENDAR);
    if (!permision.hasPermission){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_CALENDAR).then((value) => {
        if(value.hasPermission){ 
          const date = new Date(this.message.dateAppointment);
          this.calendar.createEventInteractively(this.message.title, '', '', date);
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder añadir la cita al calendario de tu dispositivo, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      const date = new Date(this.message.dateAppointment);
      this.calendar.createEventInteractively(this.message.title, '', '', date);
    }
  } 

  dismiss() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async deleteMessage() {
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea eliminar el mensaje?',
      buttons: [
        {text: 'Cancelar', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        const modalId = await this.utilsService.presentLoading();
        this.userService.deleteMsg(this.authService.getCurrentUserUID(), this.message.id)
        .then(() => {
          this.utilsService.showToast("Se ha borrado el mensaje correctamente", "bottom", "success");
          this.utilsService.dissmissLoading({ id: modalId.id });
          this.dismiss();
        })
        .catch((e) => this.utilsService.errorHandling(e));
      }}]
    });

    await alert.present();
  }

}
