import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterRrssPageRoutingModule } from './register-rrss-routing.module';

import { RegisterRrssPage } from './register-rrss.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    RegisterRrssPageRoutingModule,
    SharedModule
  ],
  declarations: [RegisterRrssPage]
})
export class RegisterRrssPageModule {}
