import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterRrssPage } from './register-rrss.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterRrssPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterRrssPageRoutingModule {}
