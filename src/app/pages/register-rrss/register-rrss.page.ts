import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UtilsService } from 'src/app/services/utils.service';
import { UserService } from 'src/app/services/user.service';
import { NavController, IonDatetime, IonSlides, IonSlide, IonContent, IonItemDivider, IonLabel, AlertController, ModalController } from '@ionic/angular';
import { ScheduleComponent } from 'src/app/components/schedule/schedule.component';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';
import { ServiceModel } from 'src/app/schemes/models/service.model';
import { ServiceComponent } from 'src/app/components/service/service.component';
import { take } from 'rxjs/internal/operators/take';
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/schemes/enums/category.enum';
import { TermsConditionsComponent } from 'src/app/components/terms-conditions/terms-conditions.component';

@Component({
  selector: 'app-register-rrss',
  templateUrl: './register-rrss.page.html',
  styleUrls: ['./register-rrss.page.scss'],
})
export class RegisterRrssPage implements OnInit {
  @ViewChild('myContent', {static: true}) myContent: IonContent;
  
  private registerForm: FormGroup;
  private registerFormBusiness: FormGroup;
  private duration: string;
  private codes: string[] = [];
  private rol: string = "C";
  private btn_rol: boolean = false;
  private category : string = "";
  private hoursWork : any = []
  private services : ServiceModel[] = [];
  private acceptTermsAndConditions: boolean;
  private user;
  private modal;
  private categories:  string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private utilsService: UtilsService,
    private userService: UserService,
    private navCtrl: NavController,
    private inforAlert: AlertController,
    private modalController: ModalController,
    private http: HttpClient
  ) {}

  ngOnInit(){
    this.registerForm = this.createForm();
    this.registerFormBusiness = this.createFormBusiness();
  }
  
  async ionViewWillEnter() {
    this.categories = this.utilsService.getArrayFromObject(Category);
    this.user = await this.userService.getUserById(this.authService.getCurrentUserUID());
    this.registerForm = this.createForm();
    this.http.get<[]>('../../../../assets/phones.json').pipe(take(1)).toPromise().then((codes) => {
      codes.forEach((elem: {code: string, dial_code: string, name: string}) => {
        this.codes.push(elem.code + " (" + elem.dial_code + ")");
      });
    }); 
  }

  createFormBusiness(): FormGroup {
    const nameBusinessValidator = /^[A-Za-zÀ-ÿ\u00f1\u00d1\s]+$/i;;
    return this.formBuilder.group({
      nameBusiness: ['', [Validators.required, Validators.minLength(5), Validators.pattern(nameBusinessValidator)]],
      streetBusiness: ['', [Validators.required, Validators.maxLength(50)]],
      maxVacancies: ['', [Validators.required, Validators.pattern(/^[1-9][0-9]*$/)]],
      daysBeforeCancelled: ['', [Validators.pattern(/^[1-9][0-9]*$/)]],
      daysPenalitation: ['', [Validators.pattern(/^[1-9][0-9]*$/)]],
    })
  }
     
  createForm(): FormGroup {
    const telephoneValidator = /^[0-9]*$/;
    return this.formBuilder.group({
      telephone: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(telephoneValidator)]],
      code: ['ES (+34)', [Validators.required]],
    })
  }

  passwordConfirming(c: AbstractControl): { invalidMatch: boolean } {
    if (c.get('password').value !== c.get('password_confirmation').value) {
      return { invalidMatch: true };
    }
  }

  async onRegister(): Promise<void>{
    // Guardamos la información y se envía a la base de datos
    if(this.rol == 'C'){
      const client = {
        telephone: this.registerForm.value.code + '_' + this.registerForm.value.telephone,
        rol: this.rol,
      }
      this.createUserOrBusiness(client);
     }else{
      const employee = {
        id: this.utilsService.generateID(),
        telephone: this.registerForm.value.code + '_' + this.registerForm.value.telephone,
        hoursWork: this.hoursWork,
        fullname: this.user.details.fullname,
        duration: this.duration,
        imgProfile: "https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Femployee.jpg?alt=media&token=b7d3cafe-e0d8-4ac6-8880-9bb1626cad99"
      } 

      this.services[0].employees = [employee.id];

      const business = {
        hoursWork: this.hoursWork, 
        telephone: this.registerForm.value.code + '_' + this.registerForm.value.telephone,

        nameBusiness: this.registerFormBusiness.value.nameBusiness,
        streetBusiness: this.registerFormBusiness.value.streetBusiness,
        daysBeforeCancelled: this.registerFormBusiness.value.daysBeforeCancelled,
        daysPenalitation: this.registerFormBusiness.value.daysPenalitation,
        maxVacancies: this.registerFormBusiness.value.maxVacancies,
        services: this.services,
        duration: this.duration,
        category: this.category,
        rol: this.rol,
        imgProfile: "https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Fgroup.jpg?alt=media&token=ecf8eead-c4ea-4a74-b032-14567c494ab3", 
      }

      this.createUserOrBusiness(business, employee);
     }
  }

  createUserOrBusiness(user, employee?: EmployeeModel){
    this.userService.createUserRRSS(this.authService.getCurrentUserUID(), user, employee)
    .then(() => this.navCtrl.navigateBack('/tabs'))
    .catch((e) => this.utilsService.errorHandling(e));
  }

  changeRol(){
    if(this.btn_rol){
      this.rol = "P";
      setTimeout(() => {
        let yOffset = document.getElementById('scroll').offsetTop-20;
        this.myContent.scrollToPoint(0,yOffset,500); 
      }, 100);
    }else{
      this.rol = "C";
    }
  }

  trackByFn(index, item) {
    return index;
  }

  async presentAlert() {
    const alert = await this.inforAlert.create({
      header: '¡Enhorabuena!',
      message: 'Se ha guardado correctamente tu horario laboral. Recuerda que si tienes diferentes operarios en tu empresa con diferentes horarios, podrás añadirlo más adelante.',
      buttons: ['OK']
    });

    await alert.present();
  }

  deleteService(service: ServiceModel){
    this.services = this.services.filter((s) => s.id !== service.id);
  }

  async saveSchedule() {
    const modal = await this.modalController.create({
      component: ScheduleComponent,
      componentProps: {
        duration: '',
        action: 'edit-profile'
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
        if(dataReturned.data.hoursWork.length != 0) {
          this.hoursWork = dataReturned.data.hoursWork;
          this.duration = dataReturned.data.duration;
          this.presentAlert();
        }
    });
    return await modal.present();
  }

  async changeService(service?: ServiceModel) {
    const modal = await this.modalController.create({
      component: ServiceComponent,
      componentProps: {
        service: service
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      if(dataReturned.data.service.length != 0) {
        const serviceUpdate = dataReturned.data.service;
        const serviceExist = this.services.some((service) => service.id == serviceUpdate.id);
        if(serviceExist) this.services = this.services.filter((service) => service.id != serviceUpdate.id);
        this.services.push(serviceUpdate);
      }
    });
    return await modal.present();
  }

  async onShowTerms() {
    this.modal = await this.modalController.create({
      component: TermsConditionsComponent
    });
    return this.modal.present();
  }

  validationFields(){
    return (this.category != '' && this.hoursWork.length != 0 && this.services.length != 0 && this.acceptTermsAndConditions);
  }
}
