import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, ModalController, AlertController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { ClientModel } from 'src/app/schemes/models/client.model';
import { UserService } from 'src/app/services/user.service';
import { TabStateService } from 'src/app/services/tab-state.service';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { ScheduleComponent } from 'src/app/components/schedule/schedule.component';
import { ServiceModel } from 'src/app/schemes/models/service.model';
import { ServiceComponent } from 'src/app/components/service/service.component';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { Category } from 'src/app/schemes/enums/category.enum';
import { AppointmentService } from 'src/app/services/appointment.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  
  private user;
  private codes: string[] = []; 
  private rol: string = '';
  private services: ServiceModel[];
  private categories : string[] = [];
  private inhabilitateHoursDuration: boolean = false;
  private category: string;
  private hoursWork = [];
  private duration: string = '';
  private editProfileForm: FormGroup;
  private email: string;

  constructor(
    private navParams: ActivatedRoute,
    private formBuilder: FormBuilder,
    private navCrtl: NavController,
    private utilsService: UtilsService,
    private userService: UserService,
    private tabsService: TabStateService,
    private modalController: ModalController,
    private scheduleComponent: ScheduleComponent,
    private http: HttpClient,
    private photoViewer: PhotoViewer,
    private appointmentService: AppointmentService
    ) {
    this.navParams.queryParams.forEach((user) => {
      this.user = user.editUser;
      this.rol = this.user.details.rol;
      this.email = user.editUser.email;
    });
   }

  ngOnInit() {
    this.categories = this.utilsService.getArrayFromObject(Category);
    this.rol == 'C' ? this.editProfileForm = this.createForm() : this.editProfileForm = this.createFormBusiness();
  }

  ionViewWillEnter() {
    this.tabsService.setDisplay(false); 
    this.rol == 'C' ? this.editProfileForm = this.createForm() : this.editProfileForm = this.createFormBusiness();
    this.http.get<[]>('../../../../assets/phones.json').pipe(take(1)).toPromise().then((codes) => {
      codes.forEach((elem: {code: string, dial_code: string, name: string}) => {
        this.codes.push(elem.code + " (" + elem.dial_code + ")");
      });
    });
  }

  createForm(): FormGroup {
    const emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const telephoneValidator = /^[0-9]*$/;
    const fullnameValidator = /^[A-Za-zÀ-ÿ\u00f1\u00d1]+(?:\s+[A-Za-zÀ-ÿ\u00f1\u00d1]+)*$/;
    return this.formBuilder.group({
      email: [this.user.email.slice(0,1).toUpperCase() + this.user.email.slice(1), [Validators.required, Validators.email, Validators.pattern(emailValidator)]],
      fullname: [this.user.details.fullname, [Validators.required, Validators.minLength(5), Validators.pattern(fullnameValidator)]],
      code: [this.user.details.telephone.split('_')[0], [Validators.required]],
      telephone: [this.user.details.telephone.split('_')[1], [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(telephoneValidator)]],
    });
  }

  createFormBusiness(): FormGroup {
    this.services = this.user.details.services;
    this.category = this.user.details.category;
    this.hoursWork = this.user.details.hoursWork;
    this.duration = this.user.details.duration;
    const emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const telephoneValidator = /^[0-9]*$/;
    const nameBusinessValidator = /^[A-Za-zÀ-ÿ\u00f1\u00d1\s]+$/i;
    return this.formBuilder.group({
      email: [this.user.email.slice(0,1).toUpperCase() + this.user.email.slice(1), [Validators.required, Validators.email, Validators.pattern(emailValidator)]],
      code: [this.user.details.telephone.split('_')[0], [Validators.required]],
      telephone: [this.user.details.telephone.split('_')[1], [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(telephoneValidator)]],
      nameBusiness: [this.user.details.nameBusiness, [Validators.required, Validators.minLength(5), Validators.pattern(nameBusinessValidator)]],
      streetBusiness: [this.user.details.streetBusiness, [Validators.required, Validators.maxLength(50)]],
      maxVacancies: [this.user.details.maxVacancies, [Validators.required, Validators.pattern(/^[1-9][0-9]*$/)]],
      daysBeforeCancelled: [this.user.details.daysBeforeCancelled ? this.user.details.daysBeforeCancelled : '', [Validators.pattern(/^[1-9][0-9]*$/)]],
      daysPenalitation: [this.user.details.daysPenalitation ? this.user.details.daysPenalitation : '', [Validators.pattern(/^[1-9][0-9]*$/)]]
    });
  }

  validationsExternal(){
    return this.category != '' && this.services.length != 0 && this.hoursWork.length != 0;
  }

  showPicture(): void{
    this.photoViewer.show(this.user.details.imgProfile);
  }

  async changeSchedule() {
    const modal = await this.modalController.create({
      component: ScheduleComponent,
      componentProps: {
        duration: this.duration,
        action: 'edit-profile'
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      this.inhabilitateHoursDuration = false;
      if(dataReturned.data.hoursWork.length != 0) {
        const hoursWorkV0 = Object.assign(this.hoursWork);
        this.hoursWork = dataReturned.data.hoursWork;

        if(dataReturned.data.duration != this.duration){
          const allAppointments = this.utilsService.getArrayFromObject(this.user.idAppointments);
          const existAppointment = allAppointments.some((object) => object.state == 'pending' && this.utilsService.getArrayFromObject(this.user.idEmployees).includes(object.id.split('+')[0]) && object.type != "A");
          if(existAppointment){
            this.hoursWork = hoursWorkV0;
            this.utilsService.showToast('¡Ups! Para poder actualizar la duración media de los servicios, debería primero asegurarse de que no dispone de ninguna reserva pendiente y volver a intentarlo', "bottom", "tertiary");
          }else{
            this.duration = dataReturned.data.duration;
            this.inhabilitateHoursDuration = true;
          }
        }
      }
    });
    return await modal.present();
  }

  async changeService(service?: ServiceModel) {
    const modal = await this.modalController.create({
      component: ServiceComponent,
      componentProps: {
        business: this.user,
        service: service
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      if(dataReturned.data.service.length != 0) {
        const serviceUpdate = dataReturned.data.service;
        const serviceExist = this.services.some((service) => service.id == serviceUpdate.id);
        if(serviceExist) this.services = this.services.filter((service) => service.id != serviceUpdate.id);
        this.services.push(serviceUpdate);
      }
    });
    return await modal.present();
  }

  async saveInformation() {
    // Se guarda y se envía toda la información
    var nameCompleted : string;
    if(this.rol == 'C') nameCompleted = this.editProfileForm.value.fullname.slice(0,1).toUpperCase()+this.editProfileForm.value.fullname.slice(1).toLowerCase();
    const userUpdate = (this.rol == 'C') ? {
      id: this.user.id,
      email: this.editProfileForm.value.email.toLowerCase(),
      details: {
        fullname: nameCompleted.replace(/\s[A-Za-zÀ-ÿ\u00f1\u00d1]/g, (l: string) => l.toUpperCase()),
        telephone: this.editProfileForm.value.code + '_' + this.editProfileForm.value.telephone,
        rol: this.rol,
        imgProfile: this.user.details.imgProfile
      }
    } : {
        id: this.user.id,
        email: this.editProfileForm.value.email.toLowerCase(),
        details: {
          telephone: this.editProfileForm.value.code + '_' + this.editProfileForm.value.telephone,
          rol: this.rol,
          imgProfile: this.user.details.imgProfile,
          nameBusiness: this.editProfileForm.value.nameBusiness.replace(/\s[A-Za-zÀ-ÿ\u00f1\u00d1]/g, (l: string) => l.toUpperCase()),
          streetBusiness: this.editProfileForm.value.streetBusiness,
          maxVacancies: this.editProfileForm.value.maxVacancies,
          daysBeforeCancelled: this.editProfileForm.value.daysBeforeCancelled,
          daysPenalitation: this.editProfileForm.value.daysPenalitation,
          services: this.services,
          category: this.category,
          duration: this.duration,
          hoursWork: this.hoursWork
        }
    }
    var updatePromisesEmployees : any = '1';
    if(this.duration != this.user.details.duration){

      const employeesHoursWorksPromises = this.utilsService.getArrayFromObject(this.user.idEmployees).map((id) => this.userService.getEmployeeById(id));
      const employees = await Promise.all(employeesHoursWorksPromises);
      
      updatePromisesEmployees = employees.map((employee: EmployeeModel) => {
        employee.hoursWork.forEach((data) => {
          const hours = this.utilsService.getArrayFromObject(data.hours);
          if(hours.length == 1) {
            hours.forEach((hour) => {
              data.hoursTotally = this.scheduleComponent.generateTagshours(hour.text.split('-')[0], hour.text.split('-')[1], hour.type, this.duration);
            });
          }else if (hours.length > 1) {
            let copy = [];
            data.hoursTotally = [];
            hours.forEach((hour) => {
              copy = this.scheduleComponent.generateTagshours(hour.text.split('-')[0], hour.text.split('-')[1], hour.type, this.duration);
              data.hoursTotally = data.hoursTotally.concat(copy);
            });
          }
        });
        
        employee.duration = this.duration;
        return this.userService.updateEmployee(employee);
      });

      if(this.inhabilitateHoursDuration){
        // Fueron cambiadas las duraciones, eliminamos las horas inhabilitadas
        const inhabilitateHours = this.utilsService.getArrayFromObject(this.user.idAppointments)
          .filter((object) => object.state == 'pending' && this.utilsService.getArrayFromObject(this.user.idEmployees).includes(object.id.split('+')[0]) && object.type == "A");
        
          if(inhabilitateHours.length > 0){
          Promise.all(inhabilitateHours.map((data) => this.appointmentService.changesProfiles(data.id, this.user.id)))
          .catch((e) => this.utilsService.errorHandling(e));
        }   
      }
    }

    this.userService.updateEditProfile(userUpdate, this.email)
      .then(() => {
        // Actualizamos la información 
        this.userService.updateEditProfileBD(userUpdate, updatePromisesEmployees)
        .then(() => {
          this.navCrtl.navigateForward('tabs/profile');
        })
        .catch((e) => this.utilsService.errorHandling(e))
      })
      .catch((e) => this.utilsService.errorHandling(e));

  }

  goBack(): void {
    this.navCrtl.pop();
  }

  deleteService(service: ServiceModel){
    if(this.services.length > 1) {
      const idAppointments = this.utilsService.getArrayFromObject(this.user.idAppointments);
      const existService = idAppointments.some((object) => (object.state == 'pending' && service.id == object.idService && object.type != 'A'));	
      if(!existService) {
        this.services = this.services.filter((services) => services.id !== service.id);
      }else{
        this.utilsService.showInformationAlert("Imposible de eliminar", "Existen reservas pendientes con este servicio", "OK");
      }
    }else{
      this.utilsService.showInformationAlert("Atención", "Debe haber mínimo un servicio en el negocio", "OK");
    }
  }

}

