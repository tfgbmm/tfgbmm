import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EditEmployeePageRoutingModule } from './edit-employee-routing.module';
import { EditEmployeePage } from './edit-employee.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    SharedModule,
    IonicSelectableModule,
    EditEmployeePageRoutingModule
  ],
  declarations: [EditEmployeePage]
})
export class EditEmployeePageModule {}
