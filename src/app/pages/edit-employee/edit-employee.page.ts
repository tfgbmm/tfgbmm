import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ScheduleComponent } from 'src/app/components/schedule/schedule.component';
import { UtilsService } from 'src/app/services/utils.service';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { ServiceModel } from 'src/app/schemes/models/service.model';
import { HttpClient } from '@angular/common/http';
import { take } from 'rxjs/internal/operators/take';
import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.page.html',
  styleUrls: ['./edit-employee.page.scss'],
})
export class EditEmployeePage implements OnInit {
  
  private codes: string[] = []; 
  private employee;
  private business;
  private editProfileForm: FormGroup;
  private addService: boolean;
  private servicesMultiple: string[];
  private promisesDeleteInhabilitates: boolean = false;
  private duration: string;
  private services: ServiceModel[];

  constructor(
    private modalController: ModalController,
    private navCrtl: NavController,
    private navParams: ActivatedRoute,
    private formBuilder: FormBuilder,
    private utilsService: UtilsService,
    private userService: UserService,
    private http: HttpClient,
    private photoViewer: PhotoViewer,
    private appointmentService: AppointmentService
    ) { 
      this.navParams.queryParams.forEach((x) => {
        if(x.user) this.employee = x.user;
        this.business = x.business;
      });
    }

  ngOnInit() {
    this.editProfileForm = this.createForm();
  }

  ionViewWillEnter(){
    this.editProfileForm = this.createForm();
    this.http.get<[]>('../../../../assets/phones.json').pipe(take(1)).toPromise().then((codes) => {
      codes.forEach((elem: {code: string, dial_code: string, name: string}) => {
        this.codes.push(elem.code + " (" + elem.dial_code + ")");
      });
    });
  }

  createForm(): FormGroup {
    const employee  = {
      hoursWork : [],
      fullname: '',
      imgProfile: 'https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Femployee.jpg?alt=media&token=b7d3cafe-e0d8-4ac6-8880-9bb1626cad99',
      telephone: 'ES (+34)_',
    }
    const telephoneValidator = /^[0-9]*$/;
    const fullnameValidator = /^[A-Za-zÀ-ÿ\u00f1\u00d1]+(?:\s+[A-Za-zÀ-ÿ\u00f1\u00d1]+)*$/;

    if(!this.employee) {
      this.addService = true;
      this.services = this.business.details.services;
      this.employee = employee;
      this.employee.id = this.utilsService.generateID();
    }
    
    return this.formBuilder.group({
      fullname: [this.employee.fullname, [Validators.required, Validators.minLength(5), Validators.pattern(fullnameValidator)]],
      code: [this.employee.telephone.split('_')[0], [Validators.required]],
      telephone: [this.employee.telephone.split('_')[1], [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(telephoneValidator)]],
    });
    
  }

  validationExternal(){
    return this.employee.hoursWork.length != 0 && this.employee.imgProfile != '';
  }

  goBack(){
    this.navCrtl.pop();
  }

  showPicture(): void{
    this.photoViewer.show(this.employee.imgProfile);
  }

  async changeSchedule() {
    const modal = await this.modalController.create({
      component: ScheduleComponent,
      componentProps: {
        duration: this.business.details.duration
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      this.promisesDeleteInhabilitates = false;
      if(dataReturned.data.hoursWork.length != 0) {
        const hoursWork = Object.assign(dataReturned.data.hoursWork);
        if(this.employee.hoursWork != hoursWork){
          const allAppointments = this.utilsService.getArrayFromObject(this.business.idAppointments);
          const appointmentsReallyPending = allAppointments.some((data) => (data.state == 'pending' && data.id.split('+')[0] == this.employee.id && data.type != 'A'));
          if(appointmentsReallyPending){
            // Hay citas pendientes con este horario
            this.utilsService.showToast('Para poder actualizar el horario del operario debes primero eliminar todas sus reservas pendientes', "bottom", "tertiary");
          }else{
            // No hay citas pendientes con clientes y por tanto se puede actualizar el horario del operario
            const existInhabilitateHours = allAppointments.filter((data) => (data.state == 'pending' && data.id.split('+')[0] == this.employee.id && data.type == 'A'));
            if(existInhabilitateHours.length > 0) {
              // Existen horas inhabilitadas y procedemos a borrarlas
              this.promisesDeleteInhabilitates = true;
            }
            this.employee.hoursWork = dataReturned.data.hoursWork;
          }
        };
      }
    });
    return await modal.present();
  }

  async saveInformation(){
    this.employee.fullname = this.editProfileForm.value.fullname;
    this.employee.telephone = this.editProfileForm.value.code + '_' + this.editProfileForm.value.telephone;

    if(this.addService) { // En caso de crear un nuevo operario comprobamos si se ha asociado a algún servicio
      const nothing = this.servicesMultiple.some((id) => id == 'nothing');
      if(!nothing) {
        this.business.details.services.map((service) => {
          if(this.servicesMultiple.includes(service.id)) service.employees.push(this.employee.id);
        });
      }  
    }

    if(this.promisesDeleteInhabilitates) { // Se borran las horas inhabilitadas si se ha cambiado su horario
      const existInhabilitate = this.utilsService.getArrayFromObject(this.business.idAppointments).filter((data) => (data.state == 'pending' && data.id.split('+')[0] == this.employee.id && data.type == 'A'));
      Promise.all(existInhabilitate.map((object) => 
        this.appointmentService.changesProfiles(object.id, this.business.id)))
          .catch((e) => this.utilsService.errorHandling(e));
    }
    
    this.userService.addOrEditEmployee(this.employee, this.business, this.addService)
      .then(() => this.navCrtl.navigateForward('/tabs/profile/employee', { queryParams: {id: this.business.id}}))
      .catch((e) => this.utilsService.errorHandling(e));
  }

}
