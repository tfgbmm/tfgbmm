import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { UtilsService } from 'src/app/services/utils.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AppointmentService } from 'src/app/services/appointment.service';
import { ServiceModel } from 'src/app/schemes/models/service.model';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-profile-employee',
  templateUrl: './profile-employee.page.html',
  styleUrls: ['./profile-employee.page.scss'],
})
export class ProfileEmployeePage {
  private employee;
  private business;
  
  constructor(
    private navParams: ActivatedRoute,
    private navController: NavController,
    private alertController: AlertController, 
    private userService: UserService,
    private utilsService: UtilsService,
    private photoViewer: PhotoViewer,
    private callNumber: CallNumber,
    private sms: SMS,
    private androidPermissions: AndroidPermissions,
    private appointmentService: AppointmentService
    ) { 
      this.navParams.queryParams.forEach((x) => {
        this.employee = x.employee;
        this.business = x.business;
      });
    }

  goBack(){
    this.navController.pop();
  }

  editEmployee(){
    this.navController.navigateForward('tabs/profile/employee/profile-employee/edit-employee', {queryParams : {user: this.employee, business: this.business}});
  }

  showPicture(): void{
    this.photoViewer.show(this.employee.imgProfile);
  }
  
  async deleteEmployee() {
    const alert = await this.alertController.create({
      header: '¿Estás seguro de que desea eliminar el operario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Cancelado');
          }
        }, {
          text: 'OK',
          handler: () => {
            this.deleteEmployeeBD();
          }
        }
      ]
    });
    
    this.utilsService.getArrayFromObject(this.business.idEmployees).length > 1 ?  
    await alert.present() : this.utilsService.showInformationAlert("Lo sentimos", "Para borrar debe disponer mínimo de dos operarios", "OK");
  }

  async deleteEmployeeBD(){
    // Comprobamos si se puede o no eliminar el operario en cuestión
    this.business.details.services.map((service: ServiceModel) => {
      service.employees = service.employees.filter((id: string) => id !== this.employee.id);
    });
    const serviceEmpty = this.business.details.services.filter((service: ServiceModel) => service.employees.length == 0);
    if (serviceEmpty.length == this.business.details.services.length) {
      this.utilsService.showToast('Parece que este operario es el único que ofrece servicios en tu negocio y es imposible de borrar, debe haber mínimo un operario ofreciendo un servicio', "bottom", "tertiary");
    }else{
      const allAppointments = this.utilsService.getArrayFromObject(this.business.idAppointments);
      const existAppointments = allAppointments.some((data) => (data.state == 'pending' && data.id.split('+')[0] == this.employee.id && data.type != 'A'));
      if (!existAppointments){
        const appointmentReallyPending = allAppointments.filter((data) => (data.state == 'pending' && data.id.split('+')[0] == this.employee.id && data.type == 'A'));
        if(appointmentReallyPending.length > 0) {
          Promise.all(appointmentReallyPending.map((data) => this.appointmentService.changesProfiles(data.id, this.business.id)))
            .catch((e) => this.utilsService.errorHandling(e));
        }
        
        this.userService.deleteEmployee(this.employee.id, this.business.id, this.business.details.services.filter((service: ServiceModel) => service.employees.length != 0))
          .then(() => this.navController.pop())
          .catch((e) => this.utilsService.errorHandling(e));
      }else{
        this.utilsService.showToast('Parece que tienes reservas pendientes con este operario. Asegúrate de cancelar todas las citas que tengas pendientes con este operario e inténtalo de nuevo', "bottom", "tertiary");
      }
    }
  }

  async contactBusiness(id: string, telephone: string){
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea contactar con el operario?',
      buttons: [
        {text: 'Cancelar', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        if(id == 'call') 
          this.sendCall(telephone);
        else if(id == 'sms') 
          this.sendSMS(telephone);
        else if(id == 'wp') 
          this.sendWhatsApp(telephone);
      }}]
    });
    await alert.present();
  }

  async sendCall(telephone: string){
    const phone: string = telephone.split('_')[0].split('(')[1].split(')')[0] + telephone.split('_')[1];
    const permision = await this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.CALL_PHONE);
    
    if (!permision.hasPermission){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE).then((value) => {
        if(value.hasPermission){ 
          this.callNumber.callNumber(phone, true)
          .catch((e) => this.utilsService.errorHandling(e));    
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder realizar una llamada, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      this.callNumber.callNumber(phone, true)
      .catch((e) => this.utilsService.errorHandling(e));
    }
  }

  async sendSMS(telephone: string){
    const phone: string = telephone.split('_')[0].split('(')[1].split(')')[0] + telephone.split('_')[1];
    const permision = await this.sms.hasPermission();
    const options = {
      android: {
        intent: 'INTENT' // send SMS with the native android SMS messaging
      }
    };

    if (!permision){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).then((value) => {
        if(value.hasPermission){ 
          this.sms.send(phone, 'Hola, me gustaría ponerme en contacto contigo.', options)
            .catch((e) => this.utilsService.errorHandling(e));
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder enviar un SMS, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      this.sms.send(phone, 'Hola, me gustaría ponerme en contacto contigo.', options)
        .catch((e) => this.utilsService.errorHandling(e));
    }
  }

  sendWhatsApp(telephone: string){
    const code = telephone.split('_')[0].split('(')[1].split(')')[0];
    const number = telephone.split('_')[1];
    window.open(`https://wa.me/${code+number}?text=Hola,%20me%20gustaría%20ponerme%20en%20contacto%20contigo.`);
  }

}
