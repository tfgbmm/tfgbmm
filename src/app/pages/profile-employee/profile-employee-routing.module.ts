import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileEmployeePage } from './profile-employee.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileEmployeePage
  },
  {
    path: 'edit-employee',
    loadChildren: () => import('../edit-employee/edit-employee.module').then( m => m.EditEmployeePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileEmployeePageRoutingModule {}
