import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { UtilsService } from 'src/app/services/utils.service';
import { NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private colorEye: string = "light";
  private booleanEye: boolean = true;
  private typeEye: string = "password";
  private loginForm: FormGroup;
  private splash: boolean = true;
  private exit: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private utils: UtilsService,
    private navController: NavController,
    private utilsService: UtilsService,
    private splashScreen: SplashScreen
  ) { }

  ngOnInit(){
    this.loginForm = this.createForm();
  }

  async ionViewWillEnter(){
    this.colorEye = "light";
    this.booleanEye = true;
    this.typeEye = "password";
    this.exit = true;
    this.loginForm = this.createForm();
    
    if(this.splash) this.splashScreen.hide();
    this.splash = false;
  }

  createForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  goAboutUs(){
    this.navController.navigateForward("login/about-us");
  }
  
  onLogin() {
    if (this.loginForm.invalid) return;
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password)
    .then(async () => {
        this.loginForm.value.email = '';
        this.loginForm.value.password = '';
        this.exit = false;
        this.router.navigate(['/tabs']);
      })
    .catch((e) => this.utils.errorHandling(e));
  }

  async onLoginFacebook() {
    const user: firebase.auth.UserCredential = await this.authService.loginWithFacebook();
    if(user) {
      this.exit = false;
      (user.additionalUserInfo.isNewUser) ? this.navController.navigateForward(['/register-rrss']) : this.router.navigate(['/tabs']);
    }
  }

  async onLoginGoogle() {
    const modalId = await this.utilsService.presentLoading();
    const user: firebase.auth.UserCredential = await this.authService.loginWithGoogle();
    this.utilsService.dissmissLoading({ id: modalId.id });
    if(user) {
      this.exit = false;
      (user.additionalUserInfo.isNewUser) ? this.navController.navigateForward(['/register-rrss']) : this.router.navigate(['/tabs']);
    }
  }

  togglePassword(){
    if(this.booleanEye) { 
      this.colorEye = "dark";
      this.booleanEye = false;
      this.typeEye = "text";
    }else{
      this.colorEye = "light";
      this.booleanEye = true;
      this.typeEye = "password";
    } 
  }
}
