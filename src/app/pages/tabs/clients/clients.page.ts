import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { UserService } from 'src/app/services/user.service';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { AuthService } from 'src/app/services/auth.service';
import { ClientModel } from 'src/app/schemes/models/client.model';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AlertController } from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.page.html',
  styleUrls: ['./clients.page.scss'],
})
export class clientsPage implements OnInit {
  
  private clients_V0: any = [];
  private clientsFilter: any = [];
  private clientsJSON;
  private loadingModal;
  private modalId;
  private showInput : Boolean = false;
  private business: BusinessModel;
  
  constructor(
    private utilsService: UtilsService,
    private userService: UserService,
    private authService: AuthService,
    private callNumber: CallNumber,
    private sms: SMS,
    private androidPermissions: AndroidPermissions,
    private photoView: PhotoViewer,
    private alertController: AlertController
    ) { }

  async ngOnInit() {
    this.modalId = await this.utilsService.presentLoading();
    this.loadingModal = true;
  }

  async ionViewWillEnter(){
    // Cargamos todos los clientes de la empresa
    this.business = await this.userService.getBusinessById(this.authService.getCurrentUserUID());
    const usersPromises = this.utilsService.getArrayFromObject(this.business.idClients).map((id: string) => this.userService.getUserById(id));
    this.clientsJSON = (await Promise.all(usersPromises)).map((user) => {
      if (user.details.rol == 'P')
        user.details['fullname'] = user.details['nameBusiness'];
        user.idBlockedBusiness = (user.idBlockedBusiness) ? 
          this.utilsService.getArrayFromObject(user.idBlockedBusiness).filter((id) => id.split('+')[0] == this.business.id) :
          [];
      return user;
    });
    this.clients_V0 = Object.assign(this.clientsJSON);
    if(this.loadingModal) { // La primera vez que entramos a la vista se muestra el loading para cargar los elementos
      this.loadingModal = false; 
      this.utilsService.dissmissLoading({id: this.modalId.id});
    }
  }

  search(){
    this.showInput = !this.showInput;
    if(!this.showInput){
      this.clientsJSON = this.clients_V0;
    }
  }

  async unlockUser(id: string){
    // Quitamos la penalización al cliente
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea quitarle la penalización al cliente?',
      buttons: [
        {text: 'No', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        var modalId = await this.utilsService.presentLoading();
        await this.userService.deleteBlockedUser(id, this.business.id)
          .then(async () => {
            this.ionViewWillEnter();
            this.utilsService.showToast("Se ha eliminado con éxito la penalización del cliente", "bottom", "success");
            await this.utilsService.dissmissLoading({ id: modalId.id })
          })
          .catch(async (e) => {
            this.utilsService.errorHandling(e);
            await this.utilsService.dissmissLoading({ id: modalId.id });
        });
      }}]
    });
    await alert.present();
  }

  filterData(filter: String){
    this.clientsFilter = this.clients_V0.filter((user) => {
      return user.details.fullname.toLowerCase().includes(filter.toLowerCase());
    });
    this.clientsJSON = (filter == "") ? this.clients_V0 : this.clientsFilter; 
  }

  trackByFn(index, item) {
    return index;
  }

  photoViewer(client: ClientModel){
    this.photoView.show(client.details.imgProfile);
  }

  async contactBusiness(id: string, telephone: string){
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea contactar con el cliente?',
      buttons: [
        {text: 'Cancelar', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        if(id == 'call') 
          this.sendCall(telephone);
        else if(id == 'sms') 
          this.sendSMS(telephone);
        else if(id == 'wp') 
          this.sendWhatsApp(telephone);
      }}]
    });
    await alert.present();
  }

  async sendCall(telephone: string){
    const phone: string = telephone.split('_')[0].split('(')[1].split(')')[0] + telephone.split('_')[1];
    const permision = await this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.CALL_PHONE);
    
    if (!permision.hasPermission){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE).then((value) => {
        if(value.hasPermission){ 
          this.callNumber.callNumber(phone, true)
            .catch((e) => this.utilsService.errorHandling(e));    
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder realizar una llamada, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      this.callNumber.callNumber(phone, true)
        .catch((e) => this.utilsService.errorHandling(e));
    }
  }

  async sendSMS(telephone: string){
    const phone: string = telephone.split('_')[0].split('(')[1].split(')')[0] + telephone.split('_')[1];
    const permision = await this.sms.hasPermission();
    const options = {
      android: {
        intent: 'INTENT' // Envía el SMS con la app nativa de SMS del dispositivo
      }
    };

    if (!permision){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).then((value) => {
        if(value.hasPermission){ 
          this.sms.send(phone, 'Hola, me gustaría ponerme en contacto contigo.', options)
            .catch((e) => this.utilsService.errorHandling(e));
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder enviar un SMS, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      this.sms.send(phone, 'Hola, me gustaría ponerme en contacto contigo.', options)
        .catch((e) => this.utilsService.errorHandling(e));
    }
  }

  sendWhatsApp(telephone: string){
    const code = telephone.split('_')[0].split('(')[1].split(')')[0];
    const number = telephone.split('_')[1];
    window.open(`https://wa.me/${code+number}?text=Hola,%20me%20gustaría%20ponerme%20en%20contacto%20contigo.`);
  }

}
