import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { clientsPageRoutingModule } from './clients-routing.module';
import { clientsPage } from './clients.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    clientsPageRoutingModule
  ],
  declarations: [clientsPage]
})
export class clientsPageModule {}
