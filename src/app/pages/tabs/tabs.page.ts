import { Component, OnInit } from '@angular/core';
import { TabStateService } from 'src/app/services/tab-state.service';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { UtilsService } from 'src/app/services/utils.service';
import { NavController } from '@ionic/angular';
import { MessageModel } from 'src/app/schemes/models/message.model';
import { Subscription } from 'rxjs/internal/Subscription';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit{

  public numMessages: number = 0;
  private messagesSubscription: Subscription;
  private rol: string;

  constructor(
    public tabStateService: TabStateService,
    public userService: UserService,
    public authService: AuthService,
    public utilsService: UtilsService,
    public navController: NavController,
    ) {}
  
  ngOnInit(){}

  ionViewWillEnter(){
    this.tabStateService.setDisplay(false);
    this.userService.getRolUser(this.authService.getCurrentUserUID())
      .then((rol) => {
        this.rol = rol;
        this.tabStateService.setDisplay(true);
      })
      .catch((e) => this.utilsService.errorHandling(e));
    // Cada vez que entre un nuevo mensaje se incrementa el valor de mensajes sin leer en el tab
    this.messagesSubscription = this.userService.getMessagesFromUserCount(this.authService.getCurrentUserUID()).subscribe((messages: MessageModel[]) => {
      let total = 0;
      messages.forEach((obj: MessageModel) => {
        if(obj.state == "unread") total++;  
      });
      this.numMessages = total;
    });  
  }

  ionViewWillLeave(){
    this.messagesSubscription.unsubscribe();
  }
  
}
