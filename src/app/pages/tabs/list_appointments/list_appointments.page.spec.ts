import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { list_appointmentsPage } from './list_appointments.page';

describe('list_appointmentsPage', () => {
  let component: list_appointmentsPage;
  let fixture: ComponentFixture<list_appointmentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ list_appointmentsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(list_appointmentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
