import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { list_appointmentsPageRoutingModule } from './list_appointments-routing.module';
import { list_appointmentsPage } from './list_appointments.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    list_appointmentsPageRoutingModule
  ],
  declarations: [list_appointmentsPage]
  
})
export class list_appointmentsPageModule {}
