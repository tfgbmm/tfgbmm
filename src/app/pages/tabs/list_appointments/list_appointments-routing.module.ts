import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { list_appointmentsPage } from './list_appointments.page';

const routes: Routes = [
  {
    path: '',
    component: list_appointmentsPage
  },
  { path: 'add-appointment', loadChildren: () =>
     import('../../add-appointment/add-appointment.module').then( m => m.AddAppointmentPageModule)
    }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class list_appointmentsPageRoutingModule {}
