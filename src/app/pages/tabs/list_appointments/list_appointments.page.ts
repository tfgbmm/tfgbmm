import { Component, OnInit, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { UtilsService } from 'src/app/services/utils.service';
import { NavController, IonContent, ModalController, IonList, AlertController, Platform } from '@ionic/angular';
import { AppointmentService } from 'src/app/services/appointment.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AppointmentModel } from 'src/app/schemes/models/appointment.model';
import { MembersComponent } from 'src/app/components/members/members.component';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
declare var require: any

@Component({
  selector: 'app-list_appointments',
  templateUrl: 'list_appointments.page.html',
  styleUrls: ['list_appointments.page.scss']
})
export class list_appointmentsPage implements OnInit{
  @ViewChild('content', {static: true}) content: IonContent;
  
  private user;
  private employees;
  private inhabilitate = false;
  private segment: string = "Pendientes";
  private dateSelected = '';
  private employee = '';
  private btnSearch = false;
  private button : boolean = true;
  private inhabilitateAppointments : AppointmentModel[];
  private inhabilitateAppointments_V0;
  private appointmentsPending : AppointmentModel[];
  private appointments : AppointmentModel[];
  private appointments_V0;
  private appointmentsPending_V0;
  private splash: boolean = true;
  private modalId;
  private subscriptionStateAppointments: Subscription;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private utilsService: UtilsService,
    private navController: NavController,
    private alertController: AlertController,
    private appointmentService: AppointmentService,
    private modalController: ModalController,
    private splashScreen: SplashScreen,
    private platform: Platform,
    private router: Router
  ) {}

  ngOnInit(){
    this.platform.resume.subscribe(() => {
      if(this.router.url == "/tabs/list_appointments") this.ionViewWillEnter();
    });

    this.platform.pause.subscribe(() => {
      if(!this.subscriptionStateAppointments.closed && this.router.url == "/tabs/list_appointments") this.subscriptionStateAppointments.unsubscribe();
    });
  }

  async ionViewWillEnter(){
    this.subscriptionStateAppointments = this.appointmentService.updateNotificationsAppointments(this.authService.getCurrentUserUID()).subscribe(async () => {
      this.dismissAnyModalAndLoadings();
      this.modalId = await this.utilsService.presentLoading();
      this.updateListAppointments();
    });
  }

  ionViewWillLeave(){
    if(!this.subscriptionStateAppointments.closed) this.subscriptionStateAppointments.unsubscribe();
  }

  async updateListAppointments(){

    if(this.splash) this.splashScreen.hide();
    this.splash = false;

    const moment = require('moment'); 
    const timestampServer = await this.utilsService.getServerTimeStamp();
    const dateNow = new Date(timestampServer['timestamp']).getTime(); 
    
    this.user = await this.userService.getUserById(this.authService.getCurrentUserUID()).catch((e) => this.utilsService.errorHandling(e));
    if (!this.user.details.rol) {
      this.navController.navigateBack('register-rrss');
      this.utilsService.dissmissLoading({ id: this.modalId.id });
    }

    if(this.user.details.rol == 'P'){
      // Se descargan todos los datos de los empreados si soy administrador de la empresa
      const employesPromises = this.utilsService.getArrayFromObject(this.user.idEmployees).map((id) => this.userService.getEmployeeById(id));
      this.employees = await Promise.all(employesPromises);
    }
    
    const allAppointmentsIds = this.utilsService.getArrayFromObject(this.user.idAppointments);
    const appointmentsIdsPending = allAppointmentsIds.filter((app) => app.state == 'pending');
    const appointmentsIdsFinished = allAppointmentsIds.filter((app) => app.state != 'pending' && app.id);

    // Comprobamos si existe alguna cita con fecha inferior a la actual
    const appointmentsDate = appointmentsIdsPending.filter((app) => {
      return new Date(app.id.split('+')[1]).getTime() < dateNow;
    });
    
    const idsUsersOrBusiness: string [] = [];
    if(appointmentsDate.length > 0) {
      // Cambiamos el estado de la citas a finalizadas las que son anteriores a la fecha actual
      const appointmentsDatePromises: any = 
        appointmentsDate.map((appoint) => 
        this.appointmentService.changeStateAppointments(this.authService.getCurrentUserUID(), appoint.id, appoint.type));

      Promise.all(appointmentsDatePromises).then(async () => {
        // Existen citas pendientes anteriores a la fecha actual
        const allAppointmentsIdsNew = await this.appointmentService.getBusinessAllAppointments(this.authService.getCurrentUserUID());
        const appointmentsIdsPendingNew = allAppointmentsIdsNew.filter((app) => app.state == 'pending');
        const appointmentsIdsFinished = allAppointmentsIdsNew.filter((app) => app.state != 'pending' && app.id);
        const appointmentsPromises = appointmentsIdsFinished.map((appoint) => this.appointmentService.getAppointmentById(appoint.id));
        const appointmentsPendingPromises = appointmentsIdsPendingNew.map((appoint) => this.appointmentService.getAppointmentById(appoint.id));
        
        // Añadimos varios campos a cada cita finalizada para la vista final
        this.appointments = (await Promise.all(appointmentsPromises)).filter((appointment:any) => {

          appointment.members = this.utilsService.getArrayFromObject(appointment.members);

          appointment.dateView = 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(0,1).toUpperCase() + 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1)
            .substring(0, moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1).length - 5);
          
          appointment.dateNumber = new Date(appointment.date).getTime();

          if(this.user.id != appointment.idBusiness && !idsUsersOrBusiness.some((id) => id == appointment.idBusiness)) idsUsersOrBusiness.push(appointment.idBusiness);
          if(this.user.id == appointment.idBusiness) {
            appointment.members.map((id) => {
              if(!idsUsersOrBusiness.some((idx) => idx == id.id)) idsUsersOrBusiness.push(id.id);
            })
          }

          if(this.user.id == appointment.idBusiness){
            let employee = this.employees.filter((employee: {id: string, fullname: string}) => (employee.id == appointment.employee.id));
            if(employee.length > 0) appointment.employee.fullname = employee[0].fullname;
          }

          return allAppointmentsIdsNew.map((object) => {
            if(object.id == appointment.id) return appointment.state = object.state;
          });
        });

        // Añadimos varios campos a cada cita pendiente para la vista final
        this.appointmentsPending = (await Promise.all(appointmentsPendingPromises)).filter((appointment:any) => {
          
          appointment.members = this.utilsService.getArrayFromObject(appointment.members);

          appointment.dateView = 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(0,1).toUpperCase() + 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1)
            .substring(0, moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1).length - 5);
          
          appointment.dateNumber = new Date(appointment.date).getTime();

          if(this.user.id != appointment.idBusiness && !idsUsersOrBusiness.some((id) => id == appointment.idBusiness)) idsUsersOrBusiness.push(appointment.idBusiness);
          if(this.user.id == appointment.idBusiness) {
            appointment.members.map((id) => {
              if(!idsUsersOrBusiness.some((idx) => idx == id.id)) idsUsersOrBusiness.push(id.id);
            })
          }

          if(this.user.id == appointment.idBusiness){
            let employee = this.employees.filter((employee: {id: string, fullname: string}) => (employee.id == appointment.employee.id));
            if(employee.length > 0) appointment.employee.fullname = employee[0].fullname;
          } 
          return appointmentsIdsPendingNew.map((object) => {
            if(object.id == appointment.id) return appointment.state = object.state;
          });
        });

        this.inhabilitateAppointments = this.appointments.concat(this.appointmentsPending).filter((app) => (app.type == 'A')).sort((a,b) => a.dateNumber-b.dateNumber);
        this.appointments = this.appointments.filter((app) => (app.type != 'A')).sort((a,b) => b.dateNumber-a.dateNumber);
        this.appointmentsPending = this.appointmentsPending.filter((app) => (app.type != 'A')).sort((a,b) => a.dateNumber-b.dateNumber);
      
        const userBusinessPromises = idsUsersOrBusiness.map((id) => this.userService.getUserById(id));
        const users = await Promise.all(userBusinessPromises);
        
        this.appointments.map((appointment) => {
          if(this.user.id == appointment.idBusiness) {
            appointment.members.forEach((member: {id: string, name: string}) => {
              let user = users.filter((user) => user.id === member.id);
              member.name = user[0].details.rol != 'P' ? user[0].details.fullname : user[0].details.nameBusiness;
            }); 
          }else{
            appointment['nameBusiness'] = users.filter((user) => user.id == appointment.idBusiness)[0].details.nameBusiness;
          }
          return appointment;
        }); 

        this.appointmentsPending.map((appointment) => {
          if(this.user.id == appointment.idBusiness) {
            appointment.members.forEach((member: {id: string, name: string}) => {
              let user = users.filter((user) => user.id === member.id);
              member.name = user[0].details.rol != 'P' ? user[0].details.fullname : user[0].details.nameBusiness;
            }); 
          }else{
            appointment['nameBusiness'] = users.filter((user) => user.id == appointment.idBusiness)[0].details.nameBusiness;
            
          }
          return appointment;
        });

        this.appointments_V0 = this.appointments;
        this.appointmentsPending_V0 =  this.appointmentsPending;
        this.inhabilitateAppointments_V0 = this.inhabilitateAppointments;
        this.utilsService.dissmissLoading({ id: this.modalId.id });
      })
      .catch((error) => this.utilsService.errorHandling(error));
    }else{
      // No existen citas pendientes anteriores a la fecha actual
      const appointmentsPromises = appointmentsIdsFinished.map((appoint) => this.appointmentService.getAppointmentById(appoint.id));
      const appointmentsPendingPromises = appointmentsIdsPending.map((appoint) => this.appointmentService.getAppointmentById(appoint.id));
      
      // Añadimos varios campos a cada cita finalizada para la vista final
      this.appointments = (await Promise.all(appointmentsPromises)).filter(async (appointment:any) => {
        appointment.members = this.utilsService.getArrayFromObject(appointment.members);

        appointment.dateView = 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(0,1).toUpperCase() + 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1)
            .substring(0, moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1).length - 5);
        
        appointment.dateNumber = new Date(appointment.date).getTime();

        if(this.user.id != appointment.idBusiness && !idsUsersOrBusiness.some((id) => id == appointment.idBusiness)) idsUsersOrBusiness.push(appointment.idBusiness);
        if(this.user.id == appointment.idBusiness) {
          appointment.members.map((id) => {
            if(!idsUsersOrBusiness.some((idx) => idx == id.id)) idsUsersOrBusiness.push(id.id);
          })
        }

        if(this.user.id == appointment.idBusiness){
          let employee = this.employees.filter((employee: {id: string, fullname: string}) => (employee.id == appointment.employee.id));
          if(employee.length > 0) appointment.employee.fullname = employee[0].fullname;
        } 
        
        return allAppointmentsIds.map((object) => {
          if(object.id == appointment.id) return appointment.state = object.state;
        });
      });

      // Añadimos varios campos a cada cita pendiente para la vista final
      this.appointmentsPending = (await Promise.all(appointmentsPendingPromises)).filter(async (appointment:any) => {
        appointment.members = this.utilsService.getArrayFromObject(appointment.members);

        appointment.dateView = 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(0,1).toUpperCase() + 
            moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1)
            .substring(0, moment(appointment.date, 'YYYY-MM-DDThh:mm').locale('es').format('LLLL').slice(1).length - 5);

        appointment.dateNumber = new Date(appointment.date).getTime();

        if(this.user.id != appointment.idBusiness && !idsUsersOrBusiness.some((id) => id == appointment.idBusiness)) idsUsersOrBusiness.push(appointment.idBusiness);
          if(this.user.id == appointment.idBusiness) {
            appointment.members.map((id) => {
              if(!idsUsersOrBusiness.some((idx) => idx == id.id)) idsUsersOrBusiness.push(id.id);
            })
          }

        if(this.user.id == appointment.idBusiness){
          let employee = this.employees.filter((employee: {id: string, fullname: string}) => (employee.id == appointment.employee.id));
          if(employee.length > 0) appointment.employee.fullname = employee[0].fullname;
        } 

        return appointmentsIdsPending.map((object) => {
          if(object.id == appointment.id) return appointment.state = object.state;
        });
      });

      this.inhabilitateAppointments = this.appointments.concat(this.appointmentsPending).filter((app) => (app.type == 'A')).sort((a,b) => a.dateNumber-b.dateNumber);
      this.appointments = this.appointments.filter((app) => (app.type != 'A')).sort((a,b) => b.dateNumber-a.dateNumber);
      this.appointmentsPending = this.appointmentsPending.filter((app) => (app.type != 'A')).sort((a,b) => a.dateNumber-b.dateNumber);

      const userBusinessPromises = idsUsersOrBusiness.map((id) => this.userService.getUserById(id));
      const users = await Promise.all(userBusinessPromises);
      
      this.appointments.map((appointment) => {
        if(this.user.id == appointment.idBusiness) {
          appointment.members.forEach((member: {id: string, name: string}) => {
            let user = users.filter((user) => user.id === member.id);
            member.name = user[0].details.rol != 'P' ? user[0].details.fullname : user[0].details.nameBusiness;
          }); 
        }else{
          appointment['nameBusiness'] = users.filter((user) => user.id == appointment.idBusiness)[0].details.nameBusiness;
          
        }
        return appointment;
      }); 

      this.appointmentsPending.map((appointment) => {
        if(this.user.id == appointment.idBusiness) {
          appointment.members.forEach((member: {id: string, name: string}) => {
            let user = users.filter((user) => user.id === member.id);
            member.name = user[0].details.rol != 'P' ? user[0].details.fullname : user[0].details.nameBusiness;
          }); 
        }else{
          appointment['nameBusiness'] = users.filter((user) => user.id == appointment.idBusiness)[0].details.nameBusiness;
          
        }
        return appointment;
      });
      
      this.appointments_V0 = this.appointments;
      this.appointmentsPending_V0 =  this.appointmentsPending;
      this.inhabilitateAppointments_V0 = this.inhabilitateAppointments;
      this.utilsService.dissmissLoading({ id: this.modalId.id });
    }

    this.toogleTypeAppointments("pending");
  }

  dismissAnyModalAndLoadings(){
    this.utilsService.dissmissModal(this.modalController);
    this.utilsService.dissmissLoading();
  }

  addInhabilitate(){
    this.navController.navigateForward("tabs/list_appointments/add-appointment", {queryParams: {id: this.user.id, user: this.user, back: "listAppointments"}});
  }

  toggleBtnSearch(){
    this.btnSearch = !this.btnSearch;
    this.changeDay();
  }

  inhabilitateToggle(){
    this.inhabilitate = !this.inhabilitate;
    this.dateSelected = '';
    this.employee = '';
    this.btnSearch = false;
    this.changeDay();
  }

  changeDayInhabilite(){
    var dateFilter: [number, number];
    if(this.dateSelected != '' && this.btnSearch) dateFilter = this.filterDate();
    this.inhabilitateAppointments = this.inhabilitateAppointments_V0;
    
    if(this.dateSelected != '') {
      this.inhabilitateAppointments = this.inhabilitateAppointments.filter((app) => {
        let date = new Date(app.date).getTime();
        return date > dateFilter[0] && date < dateFilter[1]; // Rango para filtrar cita de un día
      }); 
      if(this.employee != '') this.inhabilitateAppointments = this.inhabilitateAppointments.filter((app) => app.employee.id == this.employee);

    }else if(this.employee != ''){
      this.inhabilitateAppointments = this.inhabilitateAppointments.filter((app) => app.employee.id == this.employee);
    }
  }

  buttonToggle(word: string){
    if(word == 'me'){
      this.button = true;
      this.employee = '';
      this.dateSelected = '';
    }else{
      this.button = false;
      this.employee = '';
      this.dateSelected = '';
    }
    this.changeDay();
  }

  changeDay(){
    var dateFilter: [number, number];
    if(this.dateSelected != '' && this.btnSearch) dateFilter = this.filterDate();
    
    if(!this.btnSearch) {
      this.appointments = this.appointments_V0;
      this.appointmentsPending = this.appointmentsPending_V0;
      this.inhabilitateAppointments = this.inhabilitateAppointments_V0;
      this.dateSelected = '';
      this.employee = '';
    }else if(this.segment == 'Finalizadas' && this.btnSearch){
      this.appointments = this.appointments_V0;
      
      if(this.user.details.rol == 'P') {
        (this.button) ? 
        this.appointments = this.appointments.filter((appointment) => (appointment.idBusiness == this.user.id)) :
        this.appointments = this.appointments.filter((appointment) => (appointment.idBusiness != this.user.id));
      }
      
      if(this.dateSelected != '') {
        this.appointments = this.appointments.filter((app) => {
          let date = new Date(app.date).getTime();
          return date > dateFilter[0] && date < dateFilter[1]; // Rango para filtrar cita de un día
        }); 
        if (this.employee != '') this.appointments = this.appointments.filter((app) => app.employee.id == this.employee);
      }
    }else if(this.segment == 'Pendientes' && this.btnSearch){
      this.appointmentsPending = this.appointmentsPending_V0;
      if(this.user.details.rol == 'P') {
        (this.button) ? 
          this.appointmentsPending = this.appointmentsPending.filter((appointment) => (appointment.idBusiness == this.user.id)) :
          this.appointmentsPending = this.appointmentsPending.filter((appointment) => (appointment.idBusiness != this.user.id))
      }
      if(this.dateSelected != '') {
      this.appointmentsPending = this.appointmentsPending.filter((app) => {
        let date = new Date(app.date).getTime();
        return date > dateFilter[0] && date < dateFilter[1]; // Rango para filtrar cita de un día
      }); 
      if (this.employee != '') this.appointmentsPending = this.appointmentsPending.filter((app) => app.employee.id == this.employee);
      }
    }
  }

  changeEmployee(){
    if(this.segment == "Pendientes"){
      this.appointmentsPending = this.appointmentsPending_V0;
      this.appointmentsPending = this.appointmentsPending.filter((app) => app.employee.id == this.employee);

      if (this.dateSelected != ''){
        const dateFilter = this.filterDate();
        this.appointmentsPending = this.appointmentsPending.filter((app) => {
          let date = new Date(app.date).getTime();
          return date > dateFilter[0] && date < dateFilter[1]; // Rango para filtrar cita de un día
        }); 
      } 
    }else if (this.segment == "Finalizadas"){
      this.appointments = this.appointments_V0;
      this.appointments = this.appointments.filter((app) => app.employee.id == this.employee);
      
      if(this.dateSelected != ''){
        const dateFilter = this.filterDate();
        this.appointments = this.appointments.filter((app) => {
          let date = new Date(app.date).getTime();
          return date > dateFilter[0] && date < dateFilter[1]; // Rango para filtrar cita de un día
        }); 
      }
    } 
  }

  filterDate(): [number, number]{
    const dateMinimum = new Date(this.dateSelected.split('T')[0].toString() + ' 00:00').getTime();
    const dateMaximum = dateMinimum + 86400000; /*1 day = 8640000 ms*/
    return [dateMinimum, dateMaximum];
  }

  toogleTypeAppointments(type: string){
    if(type == "pending") {
      this.appointmentsPending = this.appointmentsPending_V0;
    }else{
      this.appointments = this.appointments_V0;
    }
    this.btnSearch = false;
    this.dateSelected = '';
    this.employee = '';
    this.button = true;
  }

  trackByFn(index, item) {
    return index;
  }

  async viewListMembers(data) {
    data.daysBerofeCancelled = this.user.details.daysBeforeCancelled;
    data.daysPenalitation = this.user.details.daysPenalitation;
    
    const modal = await this.modalController.create({
      component: MembersComponent,
      componentProps: {
        appointment: data
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      if(dataReturned.data.appointment) {
        if(!dataReturned.data.date) dataReturned.data.date = "";
        this.confirmAppointment(dataReturned.data.appointment, dataReturned.data.date);
      }
    });

    if(this.user.id == data.idBusiness) return await modal.present(); // Sólo visible por el administrador de la cita
  }

  async confirmAppointment(item: AppointmentModel, date: string){
    const modalId = await this.utilsService.presentLoading();
    this.appointmentService.confirmAppointment(item, date)
      .then(async () => {
        await this.utilsService.dissmissLoading({ id: modalId.id });
        this.utilsService.showToast("Se ha pasado la lista de asistencia correctamente", "bottom", "success");
      })
      .catch(async (e) => {
        await this.utilsService.dissmissLoading({ id: modalId.id });
        this.utilsService.errorHandling(e);
    });
  }

  async deleteInfoAlert(item: AppointmentModel) {
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea cancelar la cita?',
      buttons: [
        {text: 'No', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        this.deleteAppointment(item);
      }}]
    });

    const alertInhabilitate = await this.alertController.create({
      header: '¿Está seguro de que quiere habilitar de nuevo la hora?',
      buttons: [
        {text: 'No', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        this.deleteAppointment(item);
      }}]
    });

    (item.type == 'A') ? await alertInhabilitate.present() : await alert.present();
  }

  async deleteAppointment(item: AppointmentModel){
    // Borramos la cita y aplicamos la penlización si se encuentra activada por la empresa pertinente
    const timestampServer = await this.utilsService.getServerTimeStamp();
    const timeNow = new Date(timestampServer['timestamp']).getTime();
    const business = await this.userService.getBusinessById(item.idBusiness).catch((e) => this.utilsService.errorHandling(e));
    
    if(item.type != 'A' && this.user.id != item.idBusiness){
      
      if(business.details.daysBeforeCancelled || business.details.daysPenalitation){ 
        item.daysBeforeCancelled = business.details.daysBeforeCancelled ? business.details.daysBeforeCancelled : '';
        item.daysPenalitation = business.details.daysPenalitation ? business.details.daysPenalitation : '';
        // Se comprueba si el sistema de penalización está activado
        if(item.daysBeforeCancelled != ''){
          const time = Math.abs(new Date(item.date).getTime() - (parseInt(item.daysBeforeCancelled) * 86400000));
          if(timeNow <= time) {
            this.cancelledApointmentCompleted(item, business, timeNow, "out-range");
          }else{
            if(item.daysPenalitation != ''){
              this.alertCancelledWithPenalitation(item, business, timeNow, "daysBefore-daysPenalitation");
            }else{
              this.alertCancelledWithPenalitation(item, business, timeNow, "daysBefore");
            }
          }
        }else{
          this.alertCancelledWithPenalitation(item, business, timeNow, "daysPenalitation");
        }
      }else{
        this.cancelledApointmentCompleted(item, business, timeNow, "not-habilitate");
      }
    }else{
      this.cancelledApointmentCompleted(item, business, timeNow);
    }
  } 

  async cancelledApointmentCompleted(item, business, timeNow, state?: string){
    const modalId = await this.utilsService.presentLoading();
    this.appointmentService.cancelledAppointments(this.user, item, business, timeNow, state)
    .then(async () => {
      await this.utilsService.dissmissLoading({ id: modalId.id });
    })
    .catch(async (e) => {
      await this.utilsService.dissmissLoading({ id: modalId.id });
      this.utilsService.errorHandling(e);
    });
  }

  async alertCancelledWithPenalitation(item: AppointmentModel, business, timeNow, state) {
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea continuar?',
      message: (state == "daysBefore-daysPenalitation") ? 
      `Recuerda que ya se ha pasado el mínimo de días para cancelar gratuitamente (hasta ${item.daysBeforeCancelled} día/s antes de la fecha) 
       por lo que si cancela ahora la cita, será penalizado y no se le devolverá el pago realizado` :
       (state == "daysPenalitation") ? 
        `Recuerda que si cancela ahora la cita, será penalizado por ${item.daysPenalitation} día/s sin poder reservar de nuevo en esta empresa y no se le devolverá el pago realizado`
        :
        `Recuerda que ya se ha pasado el mínimo de días para cancelar gratuitamente (hasta ${item.daysBeforeCancelled} día/s antes de la fecha) 
        por lo que si cancela ahora la cita, no se le devolverá el pago realizado`,
        buttons: [
          {text: 'No', handler: () => {}}, 
          {text: 'Sí, estoy seguro', handler: async () => {
            this.cancelledApointmentCompleted(item, business, timeNow, state);
          }}
        ]
    });
    await alert.present();
  }

}
