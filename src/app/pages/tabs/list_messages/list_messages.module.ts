import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { list_messagesPageRoutingModule } from './list_messages-routing.module';
import { list_messagesPage } from './list_messages.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    list_messagesPageRoutingModule
  ],
  declarations: [list_messagesPage]
})
export class list_messagesPageModule {}
