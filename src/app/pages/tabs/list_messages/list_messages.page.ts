import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MessagesPage } from '../../messages/messages.page';
import { MessageModel } from 'src/app/schemes/models/message.model';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';
import { UtilsService } from 'src/app/services/utils.service';

declare var require: any
@Component({
  selector: 'app-list_messages',
  templateUrl: 'list_messages.page.html',
  styleUrls: ['list_messages.page.scss']
})
export class list_messagesPage implements OnInit{
  
  private messages : MessageModel[];
  private subscriptionMessage: Subscription; 
  private loadingModal;
  private modalId;
  private moment = require('moment');
  constructor(
    private modalController: ModalController,
    private userService: UserService,
    private authService: AuthService,
    private utilsService: UtilsService
    ) {}
  
  async ngOnInit(){
    this.modalId = await this.utilsService.presentLoading();
    // Cargamos todos los mensajes del usuario
    this.subscriptionMessage = this.userService.getMessagesFromUserCount(this.authService.getCurrentUserUID())
      .subscribe((messages: MessageModel[]) => {
        messages.map((msg) => {
          msg.subtitle = `${this.moment(msg.date).locale('es').format('dddd').toString().slice(0,1).toUpperCase()+this.moment(msg.date).locale('es').format('dddd').slice(1).toString()}, 
          ${this.moment(msg.date).locale('es').format('LL')}`;
          return msg;
        });
        
        this.messages = messages.sort((a,b) => b.date - a.date);
        this.utilsService.dissmissLoading({id: this.modalId.id});
        this.loadingModal = true; // Cargamos el loading la primera vez que se entra en la vista
      });
  }
  
  ionViewWillEnter(){
    if(this.loadingModal){
    this.subscriptionMessage = this.userService.getMessagesFromUserCount(this.authService.getCurrentUserUID())
      .subscribe((messages: MessageModel[]) => {
        messages.map((msg) => {
          msg.subtitle = `${this.moment(msg.date).locale('es').format('dddd').toString().slice(0,1).toUpperCase()+this.moment(msg.date).locale('es').format('dddd').slice(1).toString()}, 
          ${this.moment(msg.date).locale('es').format('LL')}`;
          return msg;
        });
        this.messages = messages.sort((a,b) => b.date - a.date);
      });
    }
  }

  ionViewWillLeave(){
    this.subscriptionMessage.unsubscribe();
  }

  trackByFn(index: number) {
    return index;
  }

  async presentModal(object: MessageModel) {
    const modal = await this.modalController.create({
      component: MessagesPage ,
      componentProps: { 
        message: object
      }
    });
    
    return await modal.present();
  }

  
}
