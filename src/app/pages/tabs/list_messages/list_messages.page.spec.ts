import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { list_messagesPage } from './list_messages.page';

describe('list_messagesPage', () => {
  let component: list_messagesPage;
  let fixture: ComponentFixture<list_messagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ list_messagesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(list_messagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
