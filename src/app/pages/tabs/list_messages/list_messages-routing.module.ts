import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { list_messagesPage } from './list_messages.page';

const routes: Routes = [
  {
    path: '',
    component: list_messagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class list_messagesPageRoutingModule {}
