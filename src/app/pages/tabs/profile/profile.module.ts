import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { profilePage } from './profile.page';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: 'edit-profile',
    loadChildren: () =>
      import('../../edit-profile/edit-profile.module').then(m => m.EditProfilePageModule),
  },
  {
    path: 'employee',
    loadChildren: () => 
    import('../../employee/employee.module').then(m => m.EmployeePageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('../../about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  { path: '', component: profilePage }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [profilePage]
})
export class profilePageModule {}

