import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { NavController } from '@ionic/angular';
import { ClientModel } from 'src/app/schemes/models/client.model';
import { UserService } from 'src/app/services/user.service';
import { TabStateService } from 'src/app/services/tab-state.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { UtilsService } from 'src/app/services/utils.service';
import { BusinessModel } from 'src/app/schemes/models/business.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class profilePage implements OnInit { 

  private user : ClientModel | BusinessModel;
  private loadingFirst;
  private load: boolean = true;
  
  constructor(
    private authService: AuthService,
    private navCtrl: NavController,
    private userService: UserService,
    private tabsService: TabStateService,
    private photoViewer: PhotoViewer,
    private utilsService: UtilsService
    ) { }

  async ngOnInit() {
    const modalId = await this.utilsService.presentLoading();
    this.user = await this.userService.getUserById(this.authService.getCurrentUserUID());
    this.utilsService.dissmissLoading({id: modalId.id});
    this.loadingFirst = true; // Cargamos el loading la primera vez que se entra en la vista
  }

  async ionViewWillEnter() {
    if(this.loadingFirst) {
      this.tabsService.setDisplay(true); 
      this.load = true;
      this.user = await this.userService.getUserById(this.authService.getCurrentUserUID());
    }
  }
  
  onLogout(): void{
    this.authService.logout();
  }

  goForward(){
    this.navCtrl.navigateForward("/tabs/profile/edit-profile", { queryParams: {editUser: this.user}});
  }

  goAboutUs(){
    this.navCtrl.navigateForward("/tabs/profile/about-us");
  }

  showPicture(): void{
    this.photoViewer.show(this.user.details.imgProfile);
  }

  trackByFn(index, item) {
    return index;
  }

  employees(){
    this.load=false;
    this.navCtrl.navigateForward('tabs/profile/employee', { queryParams: {id: this.user.id}})
  }

}

