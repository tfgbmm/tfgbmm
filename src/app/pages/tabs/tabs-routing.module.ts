import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'list_appointments',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./list_appointments/list_appointments.module').then(m => m.list_appointmentsPageModule)
          }
        ]
      },
      {
        path: 'list_companies',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./list_companies/list_companies.module').then(m => m.list_companiesPageModule)
          }
        ]
      },
      {
        path: 'list_messages',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./list_messages/list_messages.module').then(m => m.list_messagesPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./profile/profile.module').then(m => m.profilePageModule)
          }
        ]
      },
      {
        path: 'clients',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('./clients/clients.module').then( m => m.clientsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/list_appointments',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/list_appointments',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

