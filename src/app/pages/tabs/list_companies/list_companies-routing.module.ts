import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { list_companiesPage } from './list_companies.page';

const routes: Routes = [
  {
    path: 'profile-business',
    loadChildren: () => 
      import('../../profile-business/profile-business.module').then( m => m.ProfileBusinessPageModule)
  },
  {
    path: '',
    component: list_companiesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class list_companiesPageRoutingModule {}
