import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { list_companiesPage } from './list_companies.page';

describe('list_companiesPage', () => {
  let component: list_companiesPage;
  let fixture: ComponentFixture<list_companiesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ list_companiesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(list_companiesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
