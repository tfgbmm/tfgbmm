import { Component, OnInit, ViewChild } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { TabStateService } from 'src/app/services/tab-state.service';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { NavController } from '@ionic/angular';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { Category } from 'src/app/schemes/enums/category.enum';


@Component({
  selector: 'app-list_companies',
  templateUrl: 'list_companies.page.html',
  styleUrls: ['list_companies.page.scss']
})
export class list_companiesPage implements OnInit{

  private radio_option: string = "nameBusiness";
  private user: any;
  private companies_V0: any = [];
  private companiesFavourites: any = [];
  private companiesFilter: any = [];
  private businessCompleted: BusinessModel[];
  private showInput : Boolean = false;
  private showFavourites: Boolean = false;
  private filter: string;
  private loadingModal;
  private modalId;
  private myBusiness: BusinessModel[];
  private categories: string[] = [];
  private rol: string;

  constructor(
    private utilsService: UtilsService,
    private tabsService: TabStateService,
    private authService: AuthService,
    private navCtrl: NavController,
    private userService: UserService) {}

  async ngOnInit(){
    this.modalId = await this.utilsService.presentLoading();
    this.loadingModal = true; 
  }

  async ionViewWillEnter(){
    this.categories = this.utilsService.getArrayFromObject(Category);
    this.user = await this.userService.getUserById(this.authService.getCurrentUserUID());
    
    this.userService.getIdsBusiness().then(async (business) => {
      const businessPromises = business.map((id) => this.userService.getBusinessById(id));
      this.businessCompleted = await Promise.all(businessPromises);
      
      if(this.user.details.rol == 'P'){ // Si es administrador diferenciamos su empresa de las demás en el listado
        this.myBusiness = this.businessCompleted.filter((bus) => bus.id === this.user.id);
        this.businessCompleted = this.businessCompleted.filter((bus) => bus.id !== this.user.id);
      }

      this.companies_V0 = Object.assign(this.businessCompleted);
      this.rol = this.user.details.rol;
      
      if(this.user.idFavourites) {
        this.companiesFavourites = Object.assign(this.companies_V0.filter(({id}) => this.user.idFavourites.includes(id)));
      }else{
        this.companiesFavourites = [];
      }
      if(this.loadingModal) { // Cargamos el loading la primera vez que se entra en la vista
        this.loadingModal = false; 
        this.utilsService.dissmissLoading({id: this.modalId.id});
      }
    }).catch(async (e) => {
      this.utilsService.errorHandling(e);
    });
  }

  ionViewWillLeave(): void{
    this.resetInformation();
  }

  filterMyBusiness(businessCompleted){
    const business = businessCompleted.filter((bus) => (bus.id == this.user.id));
    if(business.length > 0) return business;
  }
  
  filterOthersBusiness(businessCompleted){
    const business = businessCompleted.filter((bus) => (bus.id != this.user.id));
    if(business.length > 0) return business;
  }

  resetInformation(){
    this.showFavourites = false;
    this.showInput = false;
    this.businessCompleted = this.companies_V0;
  }
  
  search(){
    this.showInput = !this.showInput;
    if(!this.showInput && !this.showFavourites){
      this.businessCompleted = this.companies_V0;
    }else if(!this.showInput && this.showFavourites){
      this.businessCompleted = this.companiesFavourites;
    }
  }

  selectCategory(category: string){
    this.filter = category;
    this.filterData(this.filter);
  }

  selectNamebusiness(){
    this.filter = '';
    this.filterData('');
  }

  filterData(text: String){
    !this.showFavourites ? 
    this.companiesFilter = this.companies_V0.filter((business) => {
      return business.details[this.radio_option].toLowerCase().includes(text.toLowerCase());
    }) : this.companiesFilter = this.companiesFavourites.filter((business) => {
      return business.details[this.radio_option].toLowerCase().includes(text.toLowerCase());
    });

    if(text == "" && !this.showFavourites){
      this.businessCompleted = this.companies_V0; 
    }else if (text == "" && this.showFavourites){
      this.businessCompleted = this.companiesFavourites; 
    }else{
      this.businessCompleted = this.companiesFilter;
    }
  }

  toogleFavourites(show: boolean){
    this.showInput = false;
    this.showFavourites = !this.showFavourites;
    this.businessCompleted = (show) ? this.companiesFavourites : this.companies_V0;
    this.radio_option = 'nameBusiness';
    this.filter = '';
  }

  goForward(object: any){
    this.tabsService.setDisplay(false);
    this.navCtrl.navigateForward('tabs/list_companies/profile-business', {queryParams: {business: object, user: this.user}})
  }

  trackByFn(index, item) {
    return index;
  }

}
