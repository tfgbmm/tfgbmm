import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { list_companiesPageRoutingModule } from './list_companies-routing.module';
import { list_companiesPage } from './list_companies.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    list_companiesPageRoutingModule
  ],
  declarations: [list_companiesPage]
})
export class list_companiesPageModule {}
