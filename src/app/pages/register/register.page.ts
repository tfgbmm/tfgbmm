import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { UtilsService } from 'src/app/services/utils.service';
import { UserService } from 'src/app/services/user.service';
import { NavController, IonContent, AlertController, ModalController} from '@ionic/angular';
import { ScheduleComponent } from 'src/app/components/schedule/schedule.component';
import { ClientModel } from 'src/app/schemes/models/client.model';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { TermsConditionsComponent } from 'src/app/components/terms-conditions/terms-conditions.component';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';
import { ServiceComponent } from 'src/app/components/service/service.component';
import { ServiceModel } from 'src/app/schemes/models/service.model';
import { take } from 'rxjs/internal/operators/take';
import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/schemes/enums/category.enum';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild('myContent', {static: true}) myContent: IonContent;

  private registerForm: FormGroup;
  private registerFormBusiness: FormGroup;
  private duration: string;
  private pass : boolean = false;
  private repeatPass : boolean = false;
  private codes: string[] = [];
  private rol: string = "C";
  private btn_rol: boolean = false;
  private category : string = "";
  private hoursWork : any = []
  private services : ServiceModel[] = [];
  private modal;
  private idEmployee: string;
  private type: string = 'password';
  private color: string = 'light';
  private repeatType: string = 'password';
  private repeatColor: string = 'light';
  private categories: string[] = [];
  private acceptTermsAndConditions: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private utilsService: UtilsService,
    private userService: UserService,
    private navCtrl: NavController,
    private inforAlert: AlertController,
    private modalController: ModalController,
    private http: HttpClient
  ) { }

  ngOnInit(){
    this.registerForm = this.createForm();
    this.registerFormBusiness = this.createFormBusiness();
  }
  
  ionViewWillEnter() {
    this.categories = this.utilsService.getArrayFromObject(Category);
    this.idEmployee = this.utilsService.generateID();
    this.registerForm = this.createForm();
    this.http.get<[]>('../../../../assets/phones.json').pipe(take(1)).toPromise().then((codes) => {
      codes.forEach((elem: {code: string, dial_code: string, name: string}) => {
        this.codes.push(elem.code + " (" + elem.dial_code + ")");
      });
    }); 
  }

  goBack(){
    this.navCtrl.pop();
  }

  createFormBusiness(): FormGroup {
    const nameBusinessValidator = /^[A-Za-zÀ-ÿ\u00f1\u00d1\s]+$/i;
    return this.formBuilder.group({
      nameBusiness: ['', [Validators.required, Validators.minLength(5), Validators.pattern(nameBusinessValidator)]],
      streetBusiness: ['', [Validators.required, Validators.maxLength(50)]],
      maxVacancies: ['', [Validators.required, Validators.pattern(/^[1-9][0-9]*$/)]],
      daysBeforeCancelled: ['', [Validators.pattern(/^[1-9][0-9]*$/)]],
      daysPenalitation: ['', [Validators.pattern(/^[1-9][0-9]*$/)]],
    })
  }

  createForm(): FormGroup {
    const emailValidator = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const telephoneValidator = /^[0-9]*$/;

    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern(emailValidator)]],
      passwords: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(8)]],
        password_confirmation: ['']
      }, { validator: this.passwordConfirming }),
      fullname: ['', [Validators.required, Validators.minLength(5), Validators.pattern(/^[A-Za-zÀ-ÿ\u00f1\u00d1]+(?:\s+[A-Za-zÀ-ÿ\u00f1\u00d1]+)*$/)]],
      telephone: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(telephoneValidator)]],
      code: ['ES (+34)', [Validators.required]],
    })
  }

  passwordConfirming(c: AbstractControl): { invalidMatch: boolean } {
    if (c.get('password').value !== c.get('password_confirmation').value) {
      return { invalidMatch: true };
    }
  }

  tooglePassword(){
    if(this.pass){
      this.pass = false;
      this.type = 'password';
      this.color = 'light';
    }else{
      this.pass = true;
      this.type = 'text';
      this.color = 'dark';
    }
  }

  tooglePasswordConfirm(){
    if(this.repeatPass){
      this.repeatPass = false;
      this.repeatType = 'password';
      this.repeatColor = 'light';
    }else{
      this.repeatPass = true;
      this.repeatType = 'text';
      this.repeatColor = 'dark';
    }
  }
 
  async onRegister(): Promise<void>{
    // Guardamos la información y se envía a la base de datos
    const nameCompleted = this.registerForm.value.fullname.slice(0,1).toUpperCase()+this.registerForm.value.fullname.slice(1).toLowerCase();
    if(this.rol == 'C'){
      const client = {
        id: "",
        email: this.registerForm.value.email.toLowerCase(),
        details: {
          fullname: nameCompleted.replace(/\s[A-Za-zÀ-ÿ\u00f1\u00d1]/g, (l: string) => l.toUpperCase()),
          telephone: this.registerForm.value.code + '_' + this.registerForm.value.telephone,
          rol: this.rol,
          imgProfile: "https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Fuser.jpg?alt=media&token=5b9d4e01-8974-411b-95f9-bd2d88c41c73"
        }
      }
      this.createUserOrBusiness(client);
    
    }else{

      const employee = {
        id: this.utilsService.generateID(),
        fullname: nameCompleted.replace(/\s[A-Za-zÀ-ÿ\u00f1\u00d1]/g, (l: string) => l.toUpperCase()),
        hoursWork: this.hoursWork,
        duration: this.duration,
        imgProfile: "https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Femployee.jpg?alt=media&token=b7d3cafe-e0d8-4ac6-8880-9bb1626cad99",
        telephone: this.registerForm.value.code + '_' + this.registerForm.value.telephone
      }
      
      this.services[0].employees = [employee.id];

      const business = {
        id: this.idEmployee,
        email: this.registerForm.value.email.toLowerCase(),
        
        details: {
          hoursWork: this.hoursWork,
          telephone: this.registerForm.value.code + '_' + this.registerForm.value.telephone,
          nameBusiness: this.registerFormBusiness.value.nameBusiness,
          streetBusiness: this.registerFormBusiness.value.streetBusiness,
          daysBeforeCancelled: this.registerFormBusiness.value.daysBeforeCancelled,
          daysPenalitation: this.registerFormBusiness.value.daysPenalitation,
          maxVacancies: this.registerFormBusiness.value.maxVacancies,
          services: this.services,
          duration: this.duration,
          category: this.category,
          rol: this.rol,
          imgProfile: "https://firebasestorage.googleapis.com/v0/b/tfg-bmm.appspot.com/o/iconsFlaticon%2Fgroup.jpg?alt=media&token=ecf8eead-c4ea-4a74-b032-14567c494ab3"
        }
      }
      this.createUserOrBusiness(business, employee);
     }
  }

  async onShowTerms() {
    this.modal = await this.modalController.create({
      component: TermsConditionsComponent
    });
    return this.modal.present();
  }

  createUserOrBusiness(user: ClientModel | BusinessModel, employee?: EmployeeModel){
    this.authService.createUser(this.registerForm.value.email.toLowerCase(), this.registerForm.value.passwords.password)
      .then(async (elem) => {
        user.id = elem.user.uid;
        this.userService.createUser(user, employee).then(() => {
          this.navCtrl.navigateForward('/tabs');
        }).catch((e) => this.utilsService.errorHandling(e));
      }).catch((e) => this.utilsService.errorHandling(e));
  }

  changeRol(){
    if(this.btn_rol){
      this.rol = "P";
      setTimeout(() => {
        let yOffset = document.getElementById('scroll').offsetTop-20;
        this.myContent.scrollToPoint(0,yOffset,500); 
      }, 100);
    }else{
      this.rol = "C";
    }
  }

  trackByFn(index, item) {
    return index;
  }

  async presentAlert() {
    const alert = await this.inforAlert.create({
      header: '¡Enhorabuena!',
      message: 'Se ha guardado correctamente el horario de tu empresa. Recuerda que si tienes diferentes operarios en tu empresa con diferentes horarios, podrás añadirlo más adelante.',
      buttons: ['OK']
    });

    await alert.present();
  }

  deleteService(service: ServiceModel){
    this.services = this.services.filter((s) => s.id !== service.id);
  }

  async saveSchedule() {
    const modal = await this.modalController.create({
      component: ScheduleComponent,
      componentProps: {
        duration: '',
        action: 'edit-profile'
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      if(dataReturned.data.hoursWork.length != 0) {
        this.hoursWork = dataReturned.data.hoursWork;
        this.duration = dataReturned.data.duration;
        this.presentAlert();
      } 
    });
    return await modal.present();
  }

  async changeService(service?: ServiceModel) {
    const modal = await this.modalController.create({
      component: ServiceComponent,
      componentProps: {
        service: service
      }
    });

    modal.onDidDismiss().then(async (dataReturned) => {
      if(dataReturned.data.service.length != 0) {
        const serviceUpdate = dataReturned.data.service;
        const serviceExist = this.services.some((service) => service.id == serviceUpdate.id);
        if(serviceExist) this.services = this.services.filter((service) => service.id != serviceUpdate.id);
        this.services.push(serviceUpdate);
      }
    });
    return await modal.present();
  }

  validationFields(){
    return (this.category != '' && this.hoursWork.length != 0 && this.services.length == 1);
  }

}
