import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute, CanActivate } from '@angular/router';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { ClientModel } from 'src/app/schemes/models/client.model';
import { UserService } from 'src/app/services/user.service';
import { TabStateService } from 'src/app/services/tab-state.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { UtilsService } from 'src/app/services/utils.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-profile-business',
  templateUrl: './profile-business.page.html',
  styleUrls: ['./profile-business.page.scss'],
})
export class ProfileBusinessPage implements OnInit {
  
  private favourited: boolean;
  private business: any;
  private user;

  constructor(
    private navCrtl: NavController,
    private NavParams: ActivatedRoute,
    private alertController: AlertController,
    private userService: UserService,
    private tabsService: TabStateService,
    private callNumber: CallNumber,
    private photoViewer: PhotoViewer,
    private sms: SMS,
    private androidPermissions: AndroidPermissions,
    private utilsService: UtilsService
    ) { 
      this.NavParams.queryParams.forEach((x) => {
        this.business = x.business;
        this.user = x.user;
      });
    }

  ngOnInit() {}

  ionViewWillEnter(){
    this.tabsService.setDisplay(false);
    if(this.user.idFavourites) {
      this.favourited = this.user.idFavourites.includes(this.business.id) ? true : false;
    }else{
      this.favourited = false;
    }
  }

  async ionViewWillLeave(){ 
    // Comprobamos si se ha añadido o no a favorito la empresa, al salir de la vista
    if(!this.user.idFavourites) this.user.idFavourites = [];
    if(this.favourited && !this.user.idFavourites.includes(this.business.id)){
      if(!this.user.idFavourites) this.user.idFavourites = [];
      this.user.idFavourites.push(this.business.id);
      await this.userService.updateUsersFavourites(this.user.id, this.user.idFavourites);

    }else if(!this.favourited && this.user.idFavourites.includes(this.business.id)){
      this.user.idFavourites = this.user.idFavourites.filter(id => id != this.business.id);
      await this.userService.updateUsersFavourites(this.user.id, this.user.idFavourites);
    }
    this.tabsService.setDisplay(true);
  }

  showPicture(): void{
    this.photoViewer.show(this.business.details.imgProfile);
  }
  
  goBack(){
    this.navCrtl.navigateBack('tabs/list_companies');
  }

  async goAppointment(){
    var modalId = await this.utilsService.presentLoading();
    const blocked = this.utilsService.getArrayFromObject(this.user.idBlockedBusiness).filter((id) => id.split('+')[0] == this.business.id);
    if(blocked.length > 0){ // Se comprueba si existe alguna penalización con el cliente
      const timestampServer = await this.utilsService.getServerTimeStamp();
      var blockedDate = blocked.filter((id) => parseInt(id.split('+')[1]) <= new Date(timestampServer['timestamp']).getTime()); 
      var blockedDateOld = blocked.filter((id) => parseInt(id.split('+')[1]) > new Date(timestampServer['timestamp']).getTime());
      if (blockedDate.length > 0) { // Borramos los que ya han pasado la penalización
        const promisesBlockedDate = blockedDate.map((id) => this.userService.deleteBlockedUser(this.user.id, id.split('+')[0]));
        await Promise.all([promisesBlockedDate]);
      }
      
      if(blockedDateOld.length > 0){ // El cliente se encuentra penalizado
        const dateString = new Date(parseInt(blockedDateOld[0].split('+')[1])); 
        const date = dateString.getDate() + "/" + (dateString.getMonth()+1).toString() + "/" + dateString.getFullYear();
        this.utilsService.showInformationAlert("Penalizado", `No puedes pedir cita en esta empresa hasta el ${date}. Lo sentimos`, "OK");
        await this.utilsService.dissmissLoading({ id: modalId.id });
      }else{
        this.navCrtl.navigateForward('tabs/list_companies/profile-business/add-appointment', {queryParams: {id: this.business.id, user: this.user, back: "profileBusiness"}});
        await this.utilsService.dissmissLoading({ id: modalId.id });
      }
    }else{
      this.navCrtl.navigateForward('tabs/list_companies/profile-business/add-appointment', {queryParams: {id: this.business.id, user: this.user, back: "profileBusiness"}});
      await this.utilsService.dissmissLoading({ id: modalId.id });
    }
  }

  trackByFn(index, item) {
    return index;
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: `Se ha añadido '${this.business.details.nameBusiness}' a tu lista de favoritos`,
      buttons: ['OK']
    });

    await alert.present();
  }

  async contactBusiness(id: string, telephone: string){
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea contactar con la empresa?',
      buttons: [
        {text: 'Cancelar', handler: () => {}}, 
        {text: 'Sí, estoy seguro', handler: async () => {
        if(id == 'call') 
          this.sendCall(telephone);
        else if(id == 'sms') 
          this.sendSMS(telephone);
        else if(id == 'wp') 
          this.whatsApp(telephone);
      }}]
    });

    await alert.present();
  }

  async sendCall(telephone: string){
    const phone: string = telephone.split('_')[0].split('(')[1].split(')')[0] + telephone.split('_')[1];
    const permision = await this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.CALL_PHONE);
    
    if (!permision.hasPermission){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CALL_PHONE).then((value) => {
        if(value.hasPermission){ 
          this.callNumber.callNumber(phone, true)
            .catch((e) => this.utilsService.errorHandling(e));    
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder realizar una llamada, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      this.callNumber.callNumber(phone, true)
        .catch((e) => this.utilsService.errorHandling(e));
    }
  }

  async sendSMS(telephone: string){
    const phone: string = telephone.split('_')[0].split('(')[1].split(')')[0] + telephone.split('_')[1];
    const permision = await this.sms.hasPermission();
    const options = {
      android: {
        intent: 'INTENT' // send SMS with the native android SMS messaging
      }
    };

    if (!permision){
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS).then((value) => {
        if(value.hasPermission){ 
          this.sms.send(phone, 'Hola, me gustaría ponerme en contacto contigo.', options)
            .catch((e) => this.utilsService.errorHandling(e));
        }else{
          this.utilsService.showToast('Lo sentimos, pero para poder enviar un SMS, debe conceder los permisos pertinentes a la aplicación.', "bottom", "tertiary");
        }
      }).catch((e) => this.utilsService.errorHandling(e));
    }else{
      this.sms.send(phone, 'Hola, me gustaría ponerme en contacto contigo.', options)
        .catch((e) => this.utilsService.errorHandling(e));
    }
  }

  whatsApp(telephone: string){
    const code = telephone.split('_')[0].split('(')[1].split(')')[0];
    const number = telephone.split('_')[1];
    window.open(`https://wa.me/${code+number}?text=Hola,%20me%20gustaría%20ponerme%20en%20contacto%20contigo.`);
  }

}
