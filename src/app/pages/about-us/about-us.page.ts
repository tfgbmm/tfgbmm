import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TabStateService } from 'src/app/services/tab-state.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage {

  constructor(
    private navController: NavController,
    private tabsService: TabStateService
  ) { }
  
  ionViewWillEnter(){
    this.tabsService.setDisplay(false);
  }

  async goBack(){
    await this.navController.pop();
  }

}
