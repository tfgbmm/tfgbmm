import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  
  forgetForm: FormGroup;

  constructor(
    private navCrtl: NavController,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private utilsService: UtilsService
    ) {}

  ngOnInit() {
    this.forgetForm = this.createForm();
  }

  ionViewWillEnter(){
    this.forgetForm = this.createForm();
  }

  goBack(){
    this.navCrtl.pop();
  }

  createForm(): FormGroup {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, 
        Validators.pattern('^[A-z0-9._%+-]+@[A-z0-9.-]+\\.[A-z]{2,4}$')]]
    });
  }

  async resetPassword() { 
    this.authService.resetPassword(this.forgetForm.value.email.toLowerCase()) 
    .then(() => {
      this.navCrtl.navigateForward('login');
      this.translate.get('send_email_reset_password').subscribe(async (data) => {
        await this.utilsService.showToast(data, "bottom", "success");
      });
      this.forgetForm.value.email = '';
    })
    .catch(e => {
      this.utilsService.errorHandling(e);
    });
  }

}
