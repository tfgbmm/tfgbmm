import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonDatetime, AlertController, Events } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { UtilsService } from 'src/app/services/utils.service';
import { ActivatedRoute } from '@angular/router';
import { AppointmentService } from 'src/app/services/appointment.service';
import { AppointmentModel } from 'src/app/schemes/models/appointment.model';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { PickerController } from '@ionic/angular';
import { TabStateService } from 'src/app/services/tab-state.service';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';
import { ServiceModel } from 'src/app/schemes/models/service.model';

@Component({
  selector: 'app-add-appointment',
  templateUrl: './add-appointment.page.html',
  styleUrls: ['./add-appointment.page.scss'],
})
export class AddAppointmentPage implements OnInit {

  private navBack;
  private idBusiness: string;
  private hourSelected : string =  "";
  private hourLast: string = "";
  private dateSelected: any = "";
  private hoursWorkBusiness : any[];
  private hoursDoctor:  any[] ;
  private appointmentsBusiness: {id: string, state: string}[] = [];
  private user;
  private business: BusinessModel;
  private minDate: string;
  private services: ServiceModel[] = [];
  private serviceID: string;
  private employees_V0: EmployeeModel[] = [];
  private employeesService: EmployeeModel[] = [];
  private employeeID: string;
  private appointmentsPending : any;
  private maxVacancies;
  private type: string = '';
  private modalId;
  private finishedAppointments = [];
  
  constructor(
    private navCtrl: NavController,
    private userService: UserService,
    private utilsService: UtilsService,
    private appointmentService: AppointmentService,
    private alertController: AlertController,
    private NavParams: ActivatedRoute,
    private appointments: AppointmentService,
    private pickerCtrl: PickerController,
    private tabsService: TabStateService
  ) { this.NavParams.queryParams.forEach((x) => {
        this.idBusiness = x.id;
        this.user = x.user;
        this.navBack = x.back;
      });
    }

  ngOnInit() {}

  async ionViewWillEnter(){
    this.tabsService.setDisplay(false);
    this.modalId = await this.utilsService.presentLoading();

    const timestampServer = await this.utilsService.getServerTimeStamp();
    // Para establecer la fecha inicial del selector datetime 
    const date = (this.user.id != this.idBusiness ) ? 
        new Date(new Date(timestampServer['timestamp']).getTime()+86400000) : 
        new Date(timestampServer['timestamp']);
    
    this.dateSelected = this.toDateNormalize(date);

    this.userService.getBusinessById(this.idBusiness).then(async (business: any) => {
      
      this.business = business;
      this.appointmentsBusiness = this.utilsService.getArrayFromObject((business.idAppointments != undefined) ? 
        business.idAppointments : {});
      
      this.finishedAppointments = this.appointmentsBusiness.filter((appoint) => appoint.state != 'pending' && appoint.id).map((obj) => obj.id);
      
      const appointmentsPending = this.appointmentsBusiness.filter((appoint) => appoint.state == 'pending');
      const appointmentsPromises : any = appointmentsPending.map((appoint) => this.appointments.getAppointmentById(appoint.id));
      this.appointmentsPending = await Promise.all(appointmentsPromises);

      this.maxVacancies = business.details.maxVacancies;
      this.hoursWorkBusiness = business.details.hoursWork;

      const employeesArray = this.utilsService.getArrayFromObject(business.idEmployees);
      const employeesPromises : any = employeesArray.map((id: string) => this.userService.getEmployeeById(id));
      this.employees_V0 = await Promise.all(employeesPromises);

      this.services = business.details.services;
      this.serviceID = this.services[0].id;
      
      this.changeDayOrEmployee('firstLoad');
      await this.utilsService.dissmissLoading({ id: this.modalId.id });
    }).catch(async e => {
      await this.utilsService.dissmissLoading({ id: this.modalId.id });
      this.utilsService.errorHandling(e);
    });
  }

  ionViewWillLeave(){ 
    this.tabsService.setDisplay(true);
  }
  
  async goBack(){
    await this.utilsService.dissmissLoading({ id: this.modalId.id });
    this.tabsService.setDisplay(false);
    this.navCtrl.pop();
  }

  async presentAlertTypeAppointments() {
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea continuar?',
      message: `La cita será ${this.type=='I'?'individual':'grupal'} a las ${this.hourSelected} y tendrá un precio total de ${ this.type == 'I' ? this.services.filter((serv) => serv.id == this.serviceID)[0].price_single : this.services.filter((serv) => serv.id == this.serviceID)[0].price_group } € `,
      buttons: [
        {text: 'Cancelar', handler: () => this.cancel()},
        {text: 'Sí, estoy de acuerdo', handler: () => this.makeAppointment()}
      ]
    });

    const alertBusiness = await this.alertController.create({
      header: '¿Está seguro de que desea continuar?',
      message: `Le informamos que la hora seleccionada (${this.hourSelected}) será inhabilitada y por tanto, nadie podrá reservar. Siempre podrá habilitarla en el panel de citas. `,
      buttons: [
        {text: 'Cancelar', handler: () => this.cancel()}, 
        {text: 'Sí, estoy de acuerdo', handler: () => this.makeAppointment()}
      ]
    });

    (this.user.id == this.business.id) ? await alertBusiness.present() : await alert.present(); 
  }

  trackByFn(index: number) {
    return index;
  }

  checkDate(){
    return this.dateSelected != "";
  }

  async makeAppointment(){
    // Preparamos la cita y envíamos la información
    const datetimeCompleted = this.setTimetoString(this.dateSelected);
    const apppointmentExistOrNot = this.appointmentsPending.filter((appointment_pending: AppointmentModel) => {
      let time = appointment_pending.id.split("+")[1];
      let timeSelected = datetimeCompleted;
      return (time == timeSelected && appointment_pending.employee.id == this.employeeID); 
    });

    const service = this.services.filter((serv) => serv.id == this.serviceID);
    const durationService = this.msToHour(this.business.details.duration);
    
    if(this.user.id == this.business.id) this.type = 'A';

    var appointment : AppointmentModel = {
      id: this.employeeID + "+" + datetimeCompleted,
      type: this.type,
      nameBusiness: this.business.details.nameBusiness,
      idBusiness: this.idBusiness,
      members: {},
      employee: {id: this.employeeID, fullname: this.employees_V0.filter((employee) => employee.id === this.employeeID)[0].fullname},
      date: datetimeCompleted,
      price: (this.type == 'I') ? service[0].price_single : service[0].price_group,
      service: {id: service[0].id, name: service[0].name, duration: durationService}
    }

    if(apppointmentExistOrNot.length > 0) {
      apppointmentExistOrNot[0].members[this.user.id] = {
        id: this.user.id, name: this.user.details.fullname ? this.user.details.fullname : this.user.details.nameBusiness
      };
      appointment.members = apppointmentExistOrNot[0].members;
    }else if(this.type != 'A'){
      appointment.members = {[this.user.id] : {id: this.user.id, name: this.user.details.fullname ? this.user.details.fullname : this.user.details.nameBusiness}}
    }
    
    this.modalId = await this.utilsService.presentLoading();
    this.appointmentService
      .createAppointment(appointment, parseInt(this.maxVacancies), this.user, this.business, datetimeCompleted)
      .then(async () => {
        await this.utilsService.dissmissLoading({ id: this.modalId.id });
        (this.navBack == "profileBusiness") ? this.navCtrl.navigateBack('tabs/list_companies') : this.navCtrl.pop();
      }).catch(async (error) => { 
        await this.utilsService.dissmissLoading({ id: this.modalId.id }); 
        this.utilsService.errorHandling(error);
      });
  }
 
  loadingListHours(){
    this.resetListHours(true);

    this.appointmentsPending.forEach((appointment) => {
      this.hoursDoctor.forEach((hours) => {
        let datetime: string = this.parseDatetimetoString(hours.text);
        if(appointment.id == (this.employeeID + "+" + datetime)){
          let members = this.utilsService.getArrayFromObject(appointment.members);
          var existUser: boolean = members.some((member) => member.id == this.user.id);
          
          if((appointment.type == "G" && members.length >= parseInt(this.maxVacancies) || 
              appointment.type == "I" || appointment.type == "A" || existUser || 
              appointment.type == 'G' && this.user.id == this.business.id || appointment.service.id != this.serviceID)) {
                // En caso de que se encuentre la hora ocupada la deshabilitamos
                this.resetListHours(false, hours.text);
          }
        }
      });
    });

    // Deshabilitamos todas las horas finalizadas de la empresa
    this.hoursDoctor.map((hours) => this.parseDatetimetoString(hours.text)).forEach((date) => {
      if(this.finishedAppointments.includes(this.employeeID+"+"+date)) this.resetListHours(false, date.split("T")[1]);;
    });
  
  }

  changeDayOrEmployee(type?: string){
    if(this.hoursWorkBusiness){
      if((type == 'changeService' || type == 'firstLoad')){ 
        // Si se ha cambiado el servicio o es la primera vez que se entra en la vista
        const serviceSelected = this.services.filter((s) => s.id == this.serviceID)[0];
  
        if(this.user.id != this.idBusiness) { 
          const employeesIDs = this.employees_V0.map((employee) => employee.id);
          const employeesIDsSameService = serviceSelected.employees.filter((id) => employeesIDs.includes(id));
          this.employeesService = this.employees_V0.filter((employee) => employeesIDsSameService.includes(employee.id));
          this.employeeID = this.employeesService[0].id;
        }else{ 
          this.employeesService = this.employees_V0; // Si soy administrador debo poder seleccionar todos los empleados sin servicio
        } 
        
        if(type == 'firstLoad') {
          this.employeeID = this.employeesService[0].id;
        }
        const dateTransform = this.dateSelected.split("T")[0] + "T00:00:00";
        const employee: EmployeeModel[] = this.employeesService.filter((employee) => (this.employeeID == employee.id));
        const nameDay: string = new Date(dateTransform).toString().split(" ")[0]; // (Lunes, martes, miércoles..)
        const objectDateSelected = employee[0].hoursWork[this.checkDay(nameDay)[0]];
        this.hoursDoctor = objectDateSelected.hoursTotally ? objectDateSelected.hoursTotally : [];

        this.loadingListHours();
        this.hourLast = "";
        this.hourSelected = "";
        
      }else{
        // Se ha cambiado el operario o la fecha del selector datetime
        const dateTransform = this.dateSelected.split("T")[0] + "T00:00:00";
        const employee: EmployeeModel[] = this.employeesService.filter((employee) => (this.employeeID == employee.id));
        const nameDay: string = new Date(dateTransform).toString().split(" ")[0]; // (Lunes, martes, miércoles..)
        const objectDateSelected = employee[0].hoursWork[this.checkDay(nameDay)[0]];
        this.hoursDoctor =  objectDateSelected.hoursTotally ? objectDateSelected.hoursTotally : [];
        
        this.loadingListHours();
        this.hourLast = "";
        this.hourSelected = "";
      }
    }
  }
  
  toDateNormalize(date: Date){
    const mes = ((date.getMonth()+1) > 9) ? (date.getMonth()+1).toString()  : "0"+(date.getMonth()+1).toString();
    const dia = ((date.getDate()) > 9) ? (date.getDate()).toString(): "0"+(date.getDate()).toString();
    this.minDate = date.getFullYear().toString() + "-" + mes + "-" + dia;
    return this.minDate;
  }

  setTimetoString(date: string){
    return date.split("T")[0] + "T" + this.hourSelected.toString();
  }

  parseDatetimetoString(tagHour: string): string{
    return this.dateSelected.split('T')[0]+"T"+tagHour;
  }

  resetListHours(enable: boolean, hourDisable?: string){
    // Limpiamos y/o imprimimos las horas ocupadas
    if(enable){
      setTimeout(() => {
        this.hoursDoctor.forEach((hourCleaned) => {
          (document.getElementById(hourCleaned.text) as HTMLButtonElement).disabled = false;
          (document.getElementById(hourCleaned.text) as HTMLButtonElement ).style.setProperty("text-decoration", "none");
          (document.getElementById(hourCleaned.text) as HTMLButtonElement).style.backgroundColor = "white";
        });
      }, 10);
    }else{
      setTimeout(() => {
        (document.getElementById(hourDisable) as HTMLButtonElement).disabled = true;
        (document.getElementById(hourDisable) as HTMLButtonElement ).style.setProperty("text-decoration", "line-through");
        (document.getElementById(hourDisable) as HTMLButtonElement).style.backgroundColor = "#7044ff";
        (document.getElementById(hourDisable) as HTMLButtonElement).style.color = "#000000";
      }, 10);
    }
  }

  setTypePicker(e){
    // Seleccionamos el tipo de cita que se pueda y quiera
    this.hourSelected = e.toElement.innerText;
    document.getElementById(this.hourSelected).style.backgroundColor = "#3880ff";
    if(this.hourLast == ""){
      this.hourLast = e.toElement.innerText;
    }else{
      document.getElementById(this.hourLast).style.backgroundColor = "white";
      document.getElementById(this.hourSelected).style.backgroundColor = "#3880ff";
      this.hourLast = this.hourSelected;
    }

    const timestampDate = this.setTimetoString(this.dateSelected);
    const appointment = this.appointmentsPending.filter((appoint) => appoint.id.split('+')[1] == timestampDate);
    const appointmentEmployeeSelected = appointment.filter((app) => app.employee.id == this.employeeID);
    
    
    if(this.business.id == this.user.id){ 
      var columns = [
        {name: 'types', options: [{text: 'Inhabilitar', value: 'I'}]}
      ];
    }else if (appointment.length > 0 && appointmentEmployeeSelected.length > 0){ 
      var columns = [
        {name: 'types', options: [{text: 'Grupal', value: 'G'}]}
      ];
    }else{
      const hour = this.hoursDoctor.filter((object) => object.text == this.hourSelected);
      if (hour[0].type == 'I'){
        var columns = [{
            name: 'types',
            options: [{text: 'Individual', value: 'I'}]
        }];
      }else if(hour[0].type == 'G'){
        var columns = [{
            name: 'types',
            options: [{text: 'Grupal', value: 'G'}]
        }];
      }else if(hour[0].type == 'ANY'){
        var columns = [{
          name: 'types',
          options: [{text: 'Grupal', value: 'G'}, {text: 'Individual', value: 'I'}]
        }];
      }
    }
  
    let presentAlertConfirm: Function = async(ok: Function = (data: any) => {}, cancel: Function = () => {}) => {
      const pickerCtrl = await this.pickerCtrl.create({
          buttons: [{
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'danger',
            handler: () => {
              this.cancel();
            }
          }, {
            text: 'OK',
            handler: (data) => {
              this.type = data.types.value;  
            }
          }],
          columns: columns 
        });
        await pickerCtrl.present();
        
    }
    presentAlertConfirm();
  }

  cancel(){
    document.getElementById(this.hourSelected).style.backgroundColor = "white";
    this.hourSelected = '';
    this.type = '';
  }

  checkDay(day: string){
    let number: number;
    switch(day){
      case "Mon": number = 0;
                  break;
      case "Tue": number = 1;
                  break;
      case "Wed": number = 2;
                  break;
      case "Thu": number = 3;
                  break;
      case "Fri": number = 4;
                  break;
      case "Sat": number = 5;
                  break;
      case "Sun": number = 6;   
                  break;
    }
    return [number];
  }

  msToHour(ms: string): string{
    let hour: string;
    switch(parseInt(ms)){
      case (5*60000): hour = '5';
                      break;
      case (10*60000): 	hour = '10';
                        break;
      case (15*60000): 	hour = '15';
                        break;
      case (30*60000): 	hour = '30'; 
                        break;
      case (45*60000): 	hour = '45'; 
                        break;
      case (60*60000): 	hour = '60';
                        break;
      case (90*60000): 	hour = '90';
                        break;
      case (120*60000): hour = '120';
                        break;
      default: console.log("Different duration", hour);
                        break;
    } 
    return hour;
  }
   
}
