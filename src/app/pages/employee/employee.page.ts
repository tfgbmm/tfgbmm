import { Component, OnInit} from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { UserService } from 'src/app/services/user.service';
import { NavController } from '@ionic/angular';
import { TabStateService } from 'src/app/services/tab-state.service';
import { ActivatedRoute } from '@angular/router';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.page.html',
  styleUrls: ['./employee.page.scss'],
})
export class EmployeePage implements OnInit {

  private employees_V0: any = [];
  private employeesFilter: any = [];
  private employeesJSON;
  private showInput : Boolean = false;
  private business;
  private id: string;
  
  constructor(
    private utilsService: UtilsService,
    private userService: UserService,
    private navCrtl: NavController,
    private tabsService: TabStateService,
    private navParams: ActivatedRoute
      ) {
        this.navParams.queryParams.forEach((x) => {
          this.id = x.id;
        });
    }

  ngOnInit() {
    this.tabsService.setDisplay(false);
  }

  async ionViewWillEnter(){
    this.tabsService.setDisplay(false);
    const modalId = await this.utilsService.presentLoading();
    this.business = await this.userService.getUserById(this.id);
    const usersPromises = this.utilsService.getArrayFromObject(this.business.idEmployees).map((id: string) => this.userService.getEmployeeById(id));
    this.employeesJSON = await Promise.all(usersPromises); // Capturamos todos los operarios asociados a la empresa
    this.employees_V0 = Object.assign(this.employeesJSON);
    this.utilsService.dissmissLoading({id: modalId.id});
  }

  addOrEditEmployee(){
    this.navCrtl.navigateForward('tabs/profile/employee/edit-employee', {queryParams : {business: this.business}});
  }

  read(item: EmployeeModel){
    this.navCrtl.navigateForward('tabs/profile/employee/profile-employee', {queryParams : {employee: item, business: this.business}});
  }

  search(){
    this.showInput = !this.showInput;
    if(!this.showInput){
      this.employeesJSON = this.employees_V0;
    }
  }

  filterData(filter: String){
    this.employeesFilter = this.employees_V0.filter((user: EmployeeModel) => {
      return user.fullname.toLowerCase().includes(filter.toLowerCase());
    });
    (filter == "") ? this.employeesJSON = this.employees_V0 : this.employeesJSON = this.employeesFilter;
  }

  trackByFn(index, item) {
    return index;
  }

  goBack(){
    this.navCrtl.navigateBack('tabs/profile');
  }

}
