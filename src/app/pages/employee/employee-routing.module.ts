import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeePage } from './employee.page';

const routes: Routes = [
  {
    path: '',
    component: EmployeePage
  },
  {
    path: 'edit-employee',
    loadChildren: () => import('../edit-employee/edit-employee.module').then( m => m.EditEmployeePageModule)
  },
  {
    path: 'profile-employee',
    loadChildren: () => import('../profile-employee/profile-employee.module').then( m => m.ProfileEmployeePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeePageRoutingModule {}
