import { Component, OnInit} from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { ModalController, AlertController } from '@ionic/angular';
import { AppointmentModel } from 'src/app/schemes/models/appointment.model';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class MembersComponent implements OnInit {

  private members;
  private toogleSave = false;
  private appointment: AppointmentModel;
  private rollCall = {};

  constructor(
    private utilsService: UtilsService,
    private modalController: ModalController,
    private alertController: AlertController
  ) { }

  ngOnInit() {}

  ionViewWillEnter(){
    this.members = this.utilsService.getArrayFromObject(this.appointment.members).map((user) => {
      user.status = 'NOTHING';
      return user;
    });
  }

  rollCallUser(member: {id: string, name: string, status: string}, btn_check: string){
    member.status = btn_check;
    this.rollCall[member.id] = {id: member.id, status: btn_check};
    if(this.utilsService.getArrayFromObject(this.rollCall).length == this.members.length) this.toogleSave = true;
  }

  async savingInformation(){
    // Comprobamos si hay algún miembro que no haya asistido
    const notAssist = this.utilsService.getArrayFromObject(this.rollCall).some((user) => user.status == 'TRUE-CLOSE');
    if(notAssist){
      if(this.appointment.daysPenalitation){ // Si la empresa tiene los días de penalización activado
        const days: number = parseInt(this.appointment.daysPenalitation) * 86400000; // Nº de días de penalización * 1 día en ms
        var date = new Date(this.appointment.dateNumber+days).getTime().toString(); 
      }
    }

    await this.modalController.dismiss({appointment: this.appointment, date: date});
  }

  async goBack(){
    await this.modalController.dismiss({});
  }

  async confirmInfoAlert(){
    const alert = await this.alertController.create({
      header: '¿Está seguro de que desea continuar?',
      buttons: [{
        text: 'Cancelar',
        handler: () => {}
      }, {
        text: 'Sí, estoy seguro',
        handler: async () => {
          this.savingInformation();
        }
      }]
    });
    await alert.present();
  }
  
}
