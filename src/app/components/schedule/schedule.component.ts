import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {
  
  tags: any[] = [];
  selectedTags: any[] = [];
  duration: string = '';

  private hours : string[] = [
  "07:00","07:15","07:30","07:45","08:00","08:15","08:30","08:45","09:00","09:15","09:30","09:45",
  "10:00","10:15","10:30","10:45","11:00","11:15","11:30","11:45","12:00","12:15","12:30","12:45",
  "13:00","13:15","13:30","13:45","14:00","14:15","14:30","14:45","15:00","15:15","15:30","15:45",
  "16:00","16:15","16:30","16:45","17:00","17:15","17:30","17:45","18:00","18:15","18:30","18:45",
  "19:00","19:15","19:30","19:45","20:00","20:15","20:30","20:45","21:00","21:15","21:30","21:45",
  "22:00","22:15","22:30","22:45","23:00","23:15","23:30","23:45"];

  private days: any [] = [
    {name: 'Lunes',  intervals : [{
      start: this.hours,
      end: this.hours,
      val_start: '',
      val_end: '',
      type: ''
    }]
  }, 
    {name: 'Martes', intervals : [{
      start: this.hours,
      end: this.hours ,
      val_start: '',
      val_end: '',
      type: ''     
    }]},
    {name: 'Miércoles', intervals : [{
      start: this.hours,
      end: this.hours ,
      val_start: '',
      val_end: '',
      type: ''    
    }]},
    {name: 'Jueves', intervals : [{
      start: this.hours,
      end: this.hours ,
      val_start: '',
      val_end: '',
      type: ''    
    }]},
    {name: 'Viernes', intervals : [{
      start: this.hours,
      end: this.hours,
      val_start: '',
      val_end: '',
      type: ''      
    }]},
    {name: 'Sábado', intervals : [{
      start: this.hours,
      end: this.hours,
      val_start: '',
      val_end: '',
      type: ''     
    }]},
    {name: 'Domingo', intervals : [{
      start: this.hours,
      end: this.hours,
      val_start: '',
      val_end: '',
      type: ''     
    }]}];

  private types : {text: string, code: string}[] = 
  [{text: "Individual", code: "I"}, {text: "Grupal", code: "G"}, {text: "Cualquiera", code: "ANY"}]
  
  private hoursWork = [];
  private action: string = '';
  constructor(
    private utilsService: UtilsService,
    private modalCtrl: ModalController,
    private navParams: ActivatedRoute,
    ) { 
      this.navParams.queryParams.forEach((data) => {
        this.duration = data.duration;
        this.action = data.action;
      });
    }

  async ngOnInit() {}

  ionViewWillEnter(){
    if(!this.duration) this.duration = '';
  }

  async goBack(){
    await this.modalCtrl.dismiss({hoursWork: [], duration: ''});
  }

  selectHour(keyDay, position, keyInterval, data?){
    // Creamos el rango de horas para cada franja horaria
    var hora = data ? data.detail.value : '';
    if(position == 'start'){
      let pos;
      this.hours.filter((value, key) => {
        if(value == hora) pos = key;
      });

      this.days[keyDay]['intervals'][keyInterval]['val_end'] = '';

      let pos1;
      this.hours.filter((value, key) => {
        if(value == this.days[keyDay]['intervals'][keyInterval]['val_start']) pos1 = key;
      });
      this.days[keyDay]['intervals'][keyInterval]['end'] = this.hours.filter((val, key) => key > pos1);
      
    }else if(position == 'add'){
      hora = this.days[keyDay]['intervals'][keyInterval-1]['val_end'];
      let pos;
      this.hours.filter((value, key) => {
        if(value == hora) pos = key;
      });
      this.days[keyDay]['intervals'][keyInterval]['start'] = this.hours.filter((val, key) => key > pos);
      this.days[keyDay]['intervals'][keyInterval]['end'] = this.hours.filter((val, key) => key > pos);
    }

  }

  addInterval(key, position, keyInterval){
    // Creamos una nueva franja horaria
    const pos_array = this.days[key]['intervals'][keyInterval]['end'].length-1;
    const pos_array2 =  this.days[key]['intervals'][keyInterval]['end'].length-2;
    if(
      this.days[key]['intervals'][keyInterval]['end'][pos_array] != this.days[key]['intervals'][keyInterval]['val_end'] && 
      this.days[key]['intervals'][keyInterval]['end'][pos_array2] != this.days[key]['intervals'][keyInterval]['val_end']
    ){

      this.days[key]['intervals'].push(
        {
          start: this.hours, 
          end: this.hours,
          val_start: '',
          val_end: '',
          type: ''   
      });
      this.selectHour(key, position, (parseInt(keyInterval)+1).toString());
    }else{
      this.utilsService.showToast("Se ha llegado al máximo de horas", "bottom", "danger");
    }
  }

  deleteInterval(key, position, keyInterval){
    this.days[key]['intervals'].pop(); // Borramos la última franja horaria del día pertinente
  }

  async onCloseModal() {
    var item = 0;

    this.hoursWork = [];
    this.days.forEach((value , key) => {
      let object = {name: value.name, hours: [], hoursTotally: []};


      value.intervals.map((ob) => {
        if(ob.val_start != '' && ob.val_end != '' && ob.type != ''){
          object.hours.push({text: ob.val_start+'-'+ob.val_end, type: ob.type});
          item++;
        }
      });

      if(object.hours.length == 1) {
        object.hours.forEach((hours) => {
          object.hoursTotally = this.generateTagshours(hours.text.split('-')[0], hours.text.split('-')[1], hours.type, this.duration);
        });
      }else if (object.hours.length > 1) {
        let copy = [];
        object.hours.forEach((hours) => {
          copy = this.generateTagshours(hours.text.split('-')[0], hours.text.split('-')[1], hours.type, this.duration);
          object.hoursTotally = object.hoursTotally.concat(copy);
        });
      }
      
      this.hoursWork.push(object);

    });

    if(item == 0){
      this.utilsService.showToast('Debe establecer al menos un rango horario', 'bottom', "danger");
      this.hoursWork = [];
    }else{
      await this.modalCtrl.dismiss({hoursWork: this.hoursWork, duration: this.duration});
    }
  }

  generateTagshours(first: string, last: string, type: string, duration: string){
    // Método para generar todas las horas de trabajo
    var durationTime = 0;
    const dateFake = "2020 01 01 ";
    var hoursWork = [];
    var hourTime = new Date(dateFake+first).getTime();
    var hourLastTime = new Date(dateFake+last).getTime();
    durationTime = parseInt(duration);
    
    if(durationTime == 0) return [];

    hoursWork.push({text: first, type: type});

    while( hourLastTime >= hourTime + durationTime){
      hourTime += durationTime;
      let date = new Date(hourTime);
      let hour = date.getHours() <=9 ? "0" + date.getHours().toString() : date.getHours().toString();
      let minutes = date.getMinutes() <=9 ? "0" + date.getMinutes().toString() : date.getMinutes().toString();
      let hourCorrect = hour + ":" + minutes;
      if(hourLastTime >= hourTime + durationTime) hoursWork.push({text: hourCorrect, type: type});
    }

    return hoursWork;
  }

}
