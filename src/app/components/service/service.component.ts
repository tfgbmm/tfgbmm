import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceModel } from 'src/app/schemes/models/service.model';
import { UtilsService } from 'src/app/services/utils.service';
import { EmployeeModel } from 'src/app/schemes/models/employee.model';
import { BusinessModel } from 'src/app/schemes/models/business.model';
import { UserService } from 'src/app/services/user.service';
import { ModalController } from '@ionic/angular';
import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss'],
})
export class ServiceComponent implements OnInit {

  private serviceForm: FormGroup;
  private service: ServiceModel ;
  private business: BusinessModel;
  private employees: EmployeeModel[];
  private employeesMultiple;

  constructor(
    private formBuilder: FormBuilder,
    private navParams: ActivatedRoute,
    private utilsService: UtilsService,
    private userService: UserService,
    private modalController: ModalController,
    private Appointment: AppointmentService
  ) {
    this.navParams.queryParams.forEach((data) => {
      this.business = data.business;
      this.service = data.service;
    });
  }

  ngOnInit() {
    this.serviceForm = this.createForm();
  }

  async ionViewWillEnter(){
    if(this.business){  
      const employeesPromises = this.utilsService.getArrayFromObject(this.business.idEmployees).map((id) => this.userService.getEmployeeById(id));
      this.employees = await Promise.all(employeesPromises);
      if(this.service) this.employeesMultiple = this.service.employees;  
    }else{
      this.employees = []; 
      this.employeesMultiple = [];
    }
    this.serviceForm = this.createForm();
  }

  createForm(): FormGroup {
    const fullnameValidator = /^[A-Za-zÀ-ÿ\u00f1\u00d1]+(?:\s+[A-Za-zÀ-ÿ\u00f1\u00d1]+)*$/;
    const priceValidator = /^[0-9]*$/;
    return this.formBuilder.group({
      name: [(this.service) ? this.service.name : '', [Validators.required, Validators.minLength(5), Validators.pattern(fullnameValidator)]],
      price_single: [(this.service) ? this.service.price_single : '', [Validators.required, Validators.pattern(priceValidator)]],
      price_group: [(this.service) ? this.service.price_group : '', [Validators.required, Validators.pattern(priceValidator)]]
    });
  }

  validations(){
    return (this.employeesMultiple && this.employeesMultiple.length > 0) || (!this.business);
  }

  async onCloseModal(){
    var existsService;
    
    if(!this.service){
      existsService = false;
    }else{
      if(this.employeesMultiple.includes('any')){ // Si se han asociado todos los operarios al servicio
        this.employeesMultiple = this.employees.map((employee) => employee.id);
      }
      
      const isDifferent : boolean =  this.service.employees.some((id) => !this.employeesMultiple.includes(id));
      if(this.serviceForm.value.name == this.service.name 
        && this.serviceForm.value.price_group == this.service.price_group 
        && this.serviceForm.value.price_single == this.service.price_single){
        
          if(isDifferent) {
            const employeesIdsDifferent = this.service.employees.filter((id) => !this.employeesMultiple.includes(id));
            const appointments = this.utilsService.getArrayFromObject(this.business.idAppointments);
            existsService = appointments.some((data) => data.state == 'pending' && data.idService == this.service.id && data.type != 'A' && employeesIdsDifferent.includes(data.id.split('+')[0]));
          }else{
            existsService = false;
          }
          
      }else{
        if(this.business){
          const appointments = this.utilsService.getArrayFromObject(this.business.idAppointments);
          existsService = appointments.some((data) => data.state == 'pending' && data.idService == this.service.id && data.type != 'A');
        }
      }
    }

    if(existsService){
      this.utilsService.showInformationAlert("Lo sentimos", "No se ha podido actualizar el servicio debido a que todavía existen reservas pendientes con el mismo", "OK");
    }else{

      if(this.employeesMultiple.includes('any')){
        this.employeesMultiple = this.employees.map((employee) => employee.id);
      }
      
      if(this.service && this.business){
  
        this.service = {
          id: (this.service) ? this.service.id : this.utilsService.generateID(),
          name: this.serviceForm.value.name,
          price_single: this.serviceForm.value.price_single,
          price_group: this.serviceForm.value.price_group,
          employees: this.employeesMultiple
        }
        
        // Comprobamos si algún operario que se ha desasociado en el servicio tiene citas pendientes con este servicio
        const allAppointments = this.utilsService.getArrayFromObject(this.business.idAppointments).filter((data) => (data.state == 'pending'));
        const appointmentsPromises = allAppointments.map((data) => this.Appointment.getAppointmentById(data.id));
        const appointments = await Promise.all(appointmentsPromises);
        const appointmentsFilter = appointments.filter((app) => (app.service.id == this.service.id));
        
        const employeesWithService: boolean[] = appointmentsFilter.map((data) => {
          return this.employeesMultiple.some((id) => data.employee.id == id);
        });
        
        const result = employeesWithService.some((state) => state == false);
        (!result) ? await this.modalController.dismiss({service: this.service}) : 
          this.utilsService.showToast("No se ha podido actualizar el servicio debido a que tienes citas pendientes con dicho servicio asociadas a algunos operarios que nos has seleccionado. Para poder deshasociar el servicio de ciertos empleados primero deberías asegurarte que no queda ninguna cita pendiente con dicho/dichos operario/operarios ofreciendo este servicio", "bottom", "tertiary");
      
      }else{
        this.service = {
          id: (this.service) ? this.service.id : this.utilsService.generateID(),
          name: this.serviceForm.value.name,
          price_single: this.serviceForm.value.price_single,
          price_group: this.serviceForm.value.price_group,
          employees: this.employeesMultiple
        } 
        await this.modalController.dismiss({service: this.service});
      }
    }
  }

  async goBack(){
    await this.modalController.dismiss({service: []});
  }
}
