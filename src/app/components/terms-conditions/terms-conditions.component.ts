import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss'],
})
export class TermsConditionsComponent {
  
  constructor(
    private modalController: ModalController
  ) { }

  async onDismissDialog() {
    await this.modalController.dismiss({
      'dismissed': true
    });
  }

}
