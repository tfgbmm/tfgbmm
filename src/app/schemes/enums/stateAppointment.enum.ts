export enum StateAppointmentModel {
    pending = "pending",
    completed = "completed",
    confirmed = "confirmed",
    not_confirmed = "not_confirmed"
}