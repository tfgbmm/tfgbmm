export enum Category {
    sports_classes = "Clases deportivas",
    academic_classes = "Clases académicas"
}