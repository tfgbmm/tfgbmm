export class ServiceModel {
    id: string;
    name: string;
    price_single: number;
    price_group: number;
    employees: string[];
}