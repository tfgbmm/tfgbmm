export class EmployeeModel {
    id: string;
    hoursWork: any; 
    fullname: string;
    imgProfile: string;
    telephone: string;
    duration: string;
}
