import { stateMessageModel } from '../enums/stateMessage.enum';

export class MessageModel{
    id: string;
    state: stateMessageModel;
    title: string;
    subtitle?: string;
    content: string;
    date: number;
    type: string;
    dateAppointment: number;
}