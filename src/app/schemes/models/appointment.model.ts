import { StateAppointmentModel } from '../enums/stateAppointment.enum';

export class AppointmentModel {
    id: string;
    idBusiness: string;
    employee: {id: string, fullname: string};
    service: {id: string, name: string, duration: string};
    nameBusiness?: string;
    daysBeforeCancelled?: string;
    daysPenalitation?: string;
    date: string;
    dateNumber?: number;
    state?: StateAppointmentModel;
    type: string;
    price: number;
    members;
}