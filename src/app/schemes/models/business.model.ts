import { MessageModel } from './message.model';
import { ServiceModel } from './service.model';

export class BusinessModel {
    id: string;
    email: string;
    idEmployees?: string[];
    idAppointments?: string[];
    idFavourites?: string[];
    idBlockedBusiness?;
    idClients?: string[];
    messages?: {[id :string] : MessageModel};
    tokenDevice?: string;
    details: {
        rol: string;
        imgProfile: string;
        nameBusiness: string;
        streetBusiness: string;
        maxVacancies: string;
        daysBeforeCancelled: string;
        daysPenalitation: string;
        services: ServiceModel[];
        duration: string;
        category: string;
        telephone: string;
        hoursWork: any[];
    }    
}