import { MessageModel } from './message.model';

export class ClientModel {
    id: string;
    email: string;
    messages?: {[id :string] : MessageModel};
    idFavourites?: string[];
    idAppointments?;
    idBlockedBusiness?;
    tokenDevice?: string;
    details: {
        fullname: string;
        rol: string;
        imgProfile: string;
        telephone: string;
    }    
}
