// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBzjzgrHiOgvB3gIqnUFJ1EaH2qMR7CDvA",
    authDomain: "tfg-bmm.firebaseapp.com",
    databaseURL: "https://tfg-bmm.firebaseio.com",
    projectId: "tfg-bmm",
    storageBucket: "tfg-bmm.appspot.com",
    messagingSenderId: "626168960359",
    appId: "1:626168960359:web:3afc734fe3121e68bbcbc2",
    measurementId: "G-G6SXYE1Z6T"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
