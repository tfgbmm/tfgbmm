import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

admin.initializeApp();
// const db = admin.database(); 
const cors = require('cors')({ origin: true });

export const helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});

export const sendPush = functions
    .https.onRequest((request, response) => {
    cors(request, response, () => {
        const payload = request.body.payload;
        const token = request.body.token;
        return admin.messaging().sendToDevice(token, payload)
            .then(() => response.status(200).send())
            .catch(error => {
                console.error(error);
                response.status(500).send();
            })
    });
});

export const currentTime = functions
    .https.onRequest((request, response) => {
    cors(request, response, () => { 
        response.send({"timestamp":new Date().getTime()})
    });
})
